//
//  CommonViewController.m
//  WPWProject
//
//  Created by Mr.Lu on 13-7-27.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import "CommonViewController.h"
//#import <libkern/OSMemoryNotification.h>

@interface CommonViewController () <UIBarPositioningDelegate,UIGestureRecognizerDelegate>
{
    
}

@end

@implementation CommonViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

	if (self) {
		// Custom initialization
	}

	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    
	// 设置导航栏的背景图


    if (IOS_VERSION >= 7.0) {
//        self.view.frame = CGRectMake(0, 20, kDeviceWidth, kDeviceHeight-20);
        if ([self.navigationController.viewControllers count] >= 2) {
            self.navigationController.interactivePopGestureRecognizer.delegate=self;
        }
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        
        [self.navigationController.navigationBar setBackgroundImage:[[UIImage imageNamed:@"bar_navigation_64"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5)] forBarPosition:UIBarPositionTop  barMetrics:UIBarMetricsDefault];
        
        self.automaticallyAdjustsScrollViewInsets=YES;
        self.navigationController.navigationBar.translucent = YES;
        
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
            self.edgesForExtendedLayout = UIRectEdgeAll;
        }
        [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    }else{
        [self.navigationController.navigationBar setBackgroundImage:[[UIImage imageNamed:@"bar_navigation"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5)] forBarMetrics:UIBarMetricsDefault];
        [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    }
    
	// ios 7 导航栏遮盖问题
    
    DLog(@"%@ loaded", [self class]);
}

- (UIBarPosition)positionForBar:(id <UIBarPositioning>)bar
{
	return UIBarPositionTop;
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
//    NSLog(@"ViewController中didReceiveMemoryWarning调用");
}

- (void)configViews
{}

- (void)requestServerData
{}

- (void)setLeftNavgationBarButtonWithNormalImage:(NSString *)normalImage highImage:(NSString *)highImage withTitle:(NSString *)title
{
    [self setLeftNavgationBarButtonWithNormalImage:normalImage highImage:highImage withTitle:title withFrame:CGRectZero];
}

- (void)setLeftNavgationBarButtonWithNormalImage:(NSString *)normalImage highImage:(NSString *)highImage withTitle:(NSString *)title withFrame:(CGRect)frame
{
//    if (self.isPresent) {
        UIButton *leftButton=nil;
        if (!self.navigationItem.leftBarButtonItem) {
            leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
            
            if (!frame.size.width) {
                leftButton.frame = CGRectMake(0, 0, 45, 34);
            } else {
                leftButton.frame = frame;
            }
            [leftButton.titleLabel setFont:[UIFont systemFontOfSize:16.5]];
            [leftButton addTarget:self action:@selector(toggleLeftBtn:) forControlEvents:UIControlEventTouchUpInside];
            leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            if (IOS7==NO) {
                [leftButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
            }else{
                leftButton.contentEdgeInsets=UIEdgeInsetsMake(0, -8, 0, 8);
            }
            if (![EmptyUtility isEmpty:title]) {
                leftButton.titleEdgeInsets=UIEdgeInsetsMake(4, 0, 0, 0);
                leftButton.contentEdgeInsets=UIEdgeInsetsMake(0, 0, 0, 0);
            }
            [leftButton setTitleColor:DEFAULT_BLUE_COLOR forState:UIControlStateNormal];
            [leftButton setTitleColor:DEFAULT_LIGHT_BLUE_COLOR forState:UIControlStateHighlighted];
            [leftButton setTitleColor:DEFAULT_BLUE_COLOR forState:UIControlStateSelected];
            [leftButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
            UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
            self.navigationItem.leftBarButtonItem = leftBarButton;
        }else{
            leftButton=(UIButton *)self.navigationItem.leftBarButtonItem.customView;
        }
        
        [leftButton setImage:[UIImage imageNamed:normalImage] forState:UIControlStateNormal];
        [leftButton setImage:[UIImage imageNamed:highImage] forState:UIControlStateHighlighted];
        [leftButton setTitle:title forState:UIControlStateNormal];
//    }
}

-(void)toggleLeftBtn:(id)sender
{
    if (self.isModal==NO) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)setRightNavgationBarButtonWithNormalImage:(NSString *)normalImage highImage:(NSString *)highImage withTitle:(NSString *)title
{
    [self setRightNavgationBarButtonWithNormalImage:normalImage highImage:highImage withTitle:title withFrame:CGRectZero];
}

- (void)setRightNavgationBarButtonWithNormalImage:(NSString *)normalImage highImage:(NSString *)highImage withTitle:(NSString *)title withFrame:(CGRect)frame
{
	UIButton *rightButton=nil;

    if (!self.navigationItem.rightBarButtonItem) {
        rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        if (!frame.size.width) {
            rightButton.frame = CGRectMake(CGRectGetMinX(rightButton.frame), CGRectGetMinY(rightButton.frame), 55, 30);
        } else {
            rightButton.frame = frame;
        }
        
        rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
//        rightButton.contentVerticalAlignment=UIControlContentVerticalAlignmentBottom;
        [rightButton.titleLabel setFont:[UIFont systemFontOfSize:16.5]];
        
        [rightButton addTarget:self action:@selector(toggleRightBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        if (IOS7==NO) {
            [rightButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 10)];
        }else{
            [rightButton setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
        }
        [rightButton setTitleColor:DEFAULT_BLUE_COLOR forState:UIControlStateNormal];
        [rightButton setTitleColor:DEFAULT_LIGHT_BLUE_COLOR forState:UIControlStateHighlighted];
        [rightButton setTitleColor:DEFAULT_BLUE_COLOR forState:UIControlStateSelected];
        [rightButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
        self.navigationItem.rightBarButtonItem = rightBarButton;
    }else{
        rightButton=(UIButton *)self.navigationItem.rightBarButtonItem.customView;
    }
	if (!frame.size.width) {
        rightButton.frame = CGRectMake(CGRectGetMinX(rightButton.frame), CGRectGetMinY(rightButton.frame), 55, 30);
    } else {
        rightButton.frame = CGRectMake(CGRectGetMinX(rightButton.frame), CGRectGetMinY(rightButton.frame), CGRectGetWidth(frame), CGRectGetHeight(frame));;
    }
    
	[rightButton setImage:[UIImage imageNamed:normalImage] forState:UIControlStateNormal];
	[rightButton setImage:[UIImage imageNamed:highImage] forState:UIControlStateHighlighted];
    [rightButton setTitle:title forState:UIControlStateNormal];
}

- (void)toggleRightBtn:(id)sender
{}
- (void)setNavgationBarTextWithText:(NSString *)text Alignment:(NSTextAlignment)alignment
{
    if (self.navigationItem.titleView.tag!=1234) {
        UILabel *t = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 140, 42)];
//        t.center                        = self.navigationController.navigationBar.center;
        t.font							= [UIFont boldSystemFontOfSize:19];
        t.textColor						= [UIColor darkGrayColor];
        t.backgroundColor				= [UIColor clearColor];
        t.textAlignment					= alignment;
        t.text							= text;
        t.tag                           = 1234;
//        t.backgroundColor=[UIColor grayColor];
        [t sizeToFit];
        self.navigationItem.titleView	= t;
    }else{
        UILabel *titleLab=(UILabel *)self.navigationItem.titleView;
        titleLab.text=text;
        [titleLab sizeToFit];
    }
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //开启iOS7的滑动返回效果
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        //只有在二级页面生效
        if ([self.navigationController.viewControllers count] >= 2) {
            self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        }else{
            self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        }
    }
//    DLog(@"%@------back item",self.navigationController.navigationItem.leftBarButtonItem);
//    DLog(@"%@-----controller name",NSStringFromClass([self class]));
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
//    DLog(@"%@-----controller name",NSStringFromClass([self class]));
}

-(void)dealloc
{
//    DLog(@"%@ dealloc", [self class]);
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
//    [PushNotificationUtility actionTapBanner:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return NO;
}

- (BOOL)shouldAutorotate
{
	return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskPortrait;
}

@end
