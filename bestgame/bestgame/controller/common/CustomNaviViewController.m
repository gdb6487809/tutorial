//
//  CustomNaviViewController.m
//  navi
//
//  Created by 关 东波 on 13-11-4.
//  Copyright (c) 2013年 关 东波. All rights reserved.
//

#import "CustomNaviViewController.h"

@interface CustomNaviViewController ()

@end

@implementation CustomNaviViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.delegate = self;
    }
    return self;
}

- (id)initWithRootViewController:(UIViewController *)rootViewController
{
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        if (IOS7) {
            self.navigationBar.translucent = NO;
            [[self navigationBar] setBarTintColor:[UIColor whiteColor]];
            NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
            style.alignment = NSTextAlignmentCenter;
            
            [[self navigationBar] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor], NSParagraphStyleAttributeName: style}];
            [[self navigationBar] setBackgroundImage:[[UIImage imageNamed:@"bar_navigation_64"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5)] forBarPosition:UIBarPositionTop  barMetrics:UIBarMetricsDefault];
            [[UIApplication  sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        }
        else
        {
            [[self navigationBar] setTintColor:[UIColor blackColor]];
            [[self navigationBar] setBackgroundImage:[[UIImage imageNamed:@"bar_navigation"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5)] forBarMetrics:UIBarMetricsDefault];
            [[UIApplication  sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        }
        
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


@end
