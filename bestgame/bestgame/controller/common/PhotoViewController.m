//
//  PhotoViewController.m
//  WPWProject
//
//  Created by 关 东波 on 13-12-14.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import "PhotoViewController.h"
#import "UIView+Utility.h"

@interface PhotoViewController ()<MWPhotoBrowserDelegate>
{
    NSMutableArray *photos;
}

@end

#define kActionSheetTagSave 150

@implementation PhotoViewController

-(instancetype)initWithType:(PhotoViewControllerType)type
{
    if (self = [super initWithDelegate:self]) {
        self.state = type;
    }
    return self;
}

- (id)initWithDelegate:(id<MWPhotoBrowserDelegate>)delegate
{
    self = [super initWithDelegate:self];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initView];
    [self loadPhotos];
}

-(void)initView
{
//    [self performLayout];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.displayActionButton = NO;
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"navi_back_normal"] forState:UIControlStateNormal];
    [backBtn setImage:[UIImage imageNamed:@"navi_back_select"] forState:UIControlStateHighlighted];
//    [backBtn setTitle:@"返回" forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(doneButtonPressed:) forControlEvents:UIControlEventTouchDown];
    backBtn.frame = CGRectMake(0, 0, 55, 30);
    if (IOS7==NO) {
        [backBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    }else{
        backBtn.contentEdgeInsets=UIEdgeInsetsMake(0, -13, 0, 13);
        backBtn.titleEdgeInsets=UIEdgeInsetsMake(2, 0, 0, 0);
    }
    [backBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
    [backBtn setTitleColor:DEFAULT_BLUE_COLOR forState:UIControlStateNormal];
    backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = doneButton;
    
    if (self.state == PhotoViewControllerTypeGame) {
        UIButton *editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        editBtn.frame = CGRectMake(0, 5, 45, 44);
        [editBtn setTitle:@"保存" forState:UIControlStateNormal];
        editBtn.titleLabel.font=[UIFont systemFontOfSize:16];
        [editBtn setTitleColor:DEFAULT_BLUE_COLOR forState:UIControlStateNormal];
        editBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [editBtn addTarget:self action:@selector(savePhoto:) forControlEvents:UIControlEventTouchDown];
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc]initWithCustomView:editBtn];
        self.navigationItem.rightBarButtonItem = editButton;
    }else{
        self.navigationItem.rightBarButtonItem=nil;
    }
    UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressDidRecognized:)];
    [self.view addGestureRecognizer:longPressRecognizer];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (IOS7) {
        [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
        [self.navigationController.navigationBar setBackgroundImage:[[UIImage imageNamed:@"bar_navigation_64"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5)] forBarPosition:UIBarPositionTop  barMetrics:UIBarMetricsDefault];
    }else{
        [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
        [self.navigationController.navigationBar setBackgroundImage:[[UIImage imageNamed:@"bar_navigation"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5)] forBarMetrics:UIBarMetricsDefault];
    }
}

- (void)setNavgationBarTextWithText:(NSString *)text Alignment:(NSTextAlignment)alignment
{
	UILabel *t = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, 22)];
    
	t.font							= [UIFont boldSystemFontOfSize:19];
	t.textColor						= [UIColor darkGrayColor];
	t.backgroundColor				= [UIColor clearColor];
	t.textAlignment					= alignment;
	t.text							= text;
	self.navigationItem.titleView	= t;
}

-(void)doneButtonPressed:(id)sender
{
    if (self.state == PhotoViewControllerTypeGame) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)savePhoto:(id)sender;
{
    if (self.state == PhotoViewControllerTypeGame) {
        MWPhoto *photo = photos[self.currentIndex];
        if ([photo underlyingImage]) {
            UIImageWriteToSavedPhotosAlbum([photo underlyingImage], self, @selector(image:didFinishSavingWithError:contextInfo:),nil);
        }
    }
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo
{
    if (error){
        [[SingletonUtility instance].appDelegate.window showTips:@"保存失败"];
    }else {
        [[SingletonUtility instance].appDelegate.window showTips:@"保存成功"];
    }
}

-(void)loadPhotos
{
    photos=[[NSMutableArray alloc] init];
    if (self.state == PhotoViewControllerTypeGame) {
        for (int i=0;i<self.photoArray.count;i++) {
            NSString *string =[self.photoArray objectAtIndex:i];
            MWPhoto *photo = [MWPhoto photoWithURL:[NSURL URLWithString:string]];
            [photos addObject:photo];
        }
    }
    [self reloadData];
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
    return self.photoArray.count;
}
- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
    if (index < photos.count)
        return [photos objectAtIndex:index];
    return nil;
}

- (NSString *)photoBrowser:(MWPhotoBrowser *)photoBrowser titleForPhotoAtIndex:(NSUInteger)index
{
    if (self.state == PhotoViewControllerTypeGame){
        [self setNavgationBarTextWithText:[NSString stringWithFormat:@"%@的图片",self.name] Alignment:NSTextAlignmentCenter];
    }
    return nil;
}

-(void) longPressDidRecognized:(UILongPressGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateBegan) {
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"保存", nil];
        sheet.tag = kActionSheetTagSave;
        [sheet showInView:self.view];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (self.state == PhotoViewControllerTypeGame) {
        if (buttonIndex != actionSheet.cancelButtonIndex) {
            [self savePhoto:nil];
        }
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
    //UIStatusBarStyleDefault = 0 黑色文字，浅色背景时使用
    //UIStatusBarStyleLightContent = 1 白色文字，深色背景时使用
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
