//
//  CustomNaviViewController.h
//  navi
//
//  Created by 关 东波 on 13-11-4.
//  Copyright (c) 2013年 关 东波. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CustomNaviViewController : UINavigationController<UINavigationControllerDelegate, UIGestureRecognizerDelegate>

@end
