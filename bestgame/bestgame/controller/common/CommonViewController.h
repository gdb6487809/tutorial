//
//  CommonViewController.h
//  WPWProject
//
//  Created by Mr.Lu on 13-7-27.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import <UIKit/UIKit.h>


#define kBadgeNumberTag 22
@interface CommonViewController : UIViewController
{
    
}

@property(nonatomic,assign) BOOL isModal;//是否是独占
- (void)configViews; //配置视图
- (void)requestServerData; //请求数据

//配置导航栏视图
-(void)setLeftNavgationBarButtonWithNormalImage:(NSString *)normalImage highImage:(NSString *)highLightImage withTitle:(NSString *)title;
-(void)setLeftNavgationBarButtonWithNormalImage:(NSString *)normalImage highImage:(NSString *)HighLightImage withTitle:(NSString *)title withFrame:(CGRect)frame;
-(void)setRightNavgationBarButtonWithNormalImage:(NSString *)normalImage highImage:(NSString *)highImage withTitle:(NSString *)title;
-(void)setRightNavgationBarButtonWithNormalImage:(NSString *)normalImage highImage:(NSString *)highImage withTitle:(NSString *)title withFrame:(CGRect)frame;
-(void)toggleLeftBtn:(id)sender;
-(void)toggleRightBtn:(id)sender;

//配置导航栏字体
-(void)setNavgationBarTextWithText:(NSString *)text Alignment:(NSTextAlignment)alignment;
@end
