//
//  PhotoViewController.h
//  WPWProject
//
//  Created by 关 东波 on 13-12-14.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"

typedef NS_ENUM(NSUInteger, PhotoViewControllerType) {
    PhotoViewControllerTypeGame = 1,
};

@protocol PhotoViewControllerDelegate <NSObject>


@end

@interface PhotoViewController : MWPhotoBrowser<UIAlertViewDelegate>

@property(nonatomic,assign) PhotoViewControllerType state;//1自己    2别人     3电影      4来自聊天页面     5、头像   6、动态  7、活动上传图片
@property(nonatomic,assign) int uid;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSMutableArray *photoArray;
@property (nonatomic, weak) id<PhotoViewControllerDelegate> broserDelegate;

-(instancetype) initWithType:(PhotoViewControllerType)type;

@end
