//
//  FontTestViewController.m
//  analysis
//
//  Created by DongboGuan on 15/4/24.
//  Copyright (c) 2015年 DongboGuan. All rights reserved.
//

#import "FontTestViewController.h"
#import "UIImage+GIF.h"

@interface FontTestViewController ()

@end

@implementation FontTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self logFont];
    [self initView];
}

-(void)initView
{
    UILabel *titleLab=[[UILabel alloc] initWithFrame:CGRectMake(0, 50, kDeviceWidth, 50)];
    titleLab.textAlignment=NSTextAlignmentCenter;
    titleLab.backgroundColor=[UIColor clearColor];
    titleLab.font=[UIFont fontWithName:@"FZLTXIHK--GBK1-0" size:12];
    titleLab.text=@"给你丫的看看字体";
    titleLab.textColor=RGBACOLOR(100, 100, 100, 1);
//    [self.view addSubview:titleLab];
    
    UIImage *gifImage=[UIImage sd_animatedGIFNamed:@"a123"];
    
    UIScrollView *bgScroll=[[UIScrollView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:bgScroll];
    float lastOffsetY=5;
    for (NSInteger i=0; i<5; i++) {
        UIImageView *gifImageView1=[[UIImageView alloc] initWithFrame:CGRectMake(0, lastOffsetY, kDeviceWidth, 100)];
        gifImageView1.size=CGSizeMake(gifImage.size.width, gifImage.size.height);
        gifImageView1.image=gifImage;
        gifImageView1.center=CGPointMake(CGRectGetWidth(self.view.frame)/2, gifImageView1.center.y);
        [bgScroll addSubview:gifImageView1];
        lastOffsetY=CGRectGetMaxY(gifImageView1.frame)+5;
    }
    bgScroll.contentSize=CGSizeMake(kDeviceWidth, lastOffsetY+10);
}

-(void)logFont{
    NSArray *familyNames = [[NSArray alloc] initWithArray:[UIFont familyNames]];
    NSArray *fontNames;
    NSInteger indFamily, indFont;
    for (indFamily=0; indFamily<[familyNames count]; ++indFamily)
    {
//        NSLog(@"Family name: %@", [familyNames objectAtIndex:indFamily]);
        fontNames = [[NSArray alloc] initWithArray:
                     [UIFont fontNamesForFamilyName:
                      [familyNames objectAtIndex:indFamily]]];
        for (indFont=0; indFont<[fontNames count]; ++indFont)
        {
            NSLog(@"Font name: %@", [fontNames objectAtIndex:indFont]);
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
