//
//  MainViewController.m
//  analysis
//
//  Created by DongboGuan on 15/2/5.
//  Copyright (c) 2015年 DongboGuan. All rights reserved.
//

#import "MainViewController.h"
#import "AnalysisDetailCell.h"
#import "AnalysisModel.h"
#import "FontTestViewController.h"
#import "ShareSheetView.h"

@interface MainViewController ()<UITableViewDataSource,UITableViewDelegate,PullingRefreshTableViewDelegate,RNFrostedSidebarDelegate>
{
    PullingRefreshTableView *gameTable;
    ShareSheetView *gameShareSheet;
    NSMutableArray *gameData;
    
    NSInteger lastIndex;
}

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    [self configViews];
    [self initData];
}

-(void)initView
{
    gameTable = [[PullingRefreshTableView alloc] initWithFrame:self.view.bounds pullingDelegate:self];
    gameTable.backgroundColor=[UIColor whiteColor];
    gameTable.delegate = self;
    gameTable.dataSource = self;
    gameTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:gameTable];
}

-(void)initData
{
    gameData=[NSMutableArray array];
    [self requestInfoWithStart:0];
}

-(void)configViews
{
    [self setNavgationBarTextWithText:@"总日报表" Alignment:NSTextAlignmentCenter];
    [self setLeftNavgationBarButtonWithNormalImage:@"side_left_list" highImage:nil withTitle:nil];
    [self setRightNavgationBarButtonWithNormalImage:nil highImage:nil withTitle:@"分享"];
}

-(void)toggleLeftBtn:(id)sender
{
    NSArray *images = @[IMAGENAMED(@"side_left_home"),IMAGENAMED(@"side_left_clear"),IMAGENAMED(@"side_left_feedback"),IMAGENAMED(@"side_left_about")];
    UIColor *color=RGBACOLOR(126, 242, 195, 1);
    NSArray *colors = @[color,color,color,color,];
    NSArray *titles = @[@"首页",@"清空缓存",@"反馈",@"关于",];
    
    RNFrostedSidebar *callout = [[RNFrostedSidebar alloc] initWithImages:images titles:titles selectedIndices:nil borderColors:colors];
    callout.delegate = self;
    //    callout.showFromRight = YES;
    [callout show];
}

-(void)toggleRightBtn:(id)sender
{
    gameShareSheet=[[ShareSheetView alloc] initWithImageUrl:@"" shareFromType:SHARE_FROM_GAME];
    [gameShareSheet showShareSheet];
}

#pragma mark -- uitableviewdelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [gameData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    AnalysisModel *model = [gameData objectAtIndex:section];
    UIView *groupHeader=[[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 40)];
    groupHeader.backgroundColor=[UIColor colorWithRed:243/255.0 green:243/255.0 blue:243/255.0 alpha:1];
    groupHeader.tag=section;
    
    UIView *botBorder=[[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(groupHeader.frame)-0.5, kDeviceWidth, 0.5)];
    botBorder.backgroundColor=[UIColor colorWithRed:236/255.0 green:236/255.0 blue:236/255.0 alpha:1];;
    [groupHeader addSubview:botBorder];
    
    UILabel *titleLab=[[UILabel alloc] initWithFrame:CGRectMake(10, 0, 150, CGRectGetHeight(groupHeader.frame))];
    titleLab.backgroundColor=[UIColor clearColor];
    titleLab.font=[UIFont systemFontOfSize:14];
    titleLab.textColor=[UIColor grayColor];
    titleLab.text=model.date;
    [groupHeader addSubview:titleLab];
    
    [groupHeader addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionOpen:)]];
    return groupHeader;
}

-(UITableViewCell *)tableView:(UITableView *)mTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    AnalysisModel *model=[gameData objectAtIndex:indexPath.section];
    static NSString *identifier = @"AnalysisDetailCell";
    cell=[mTableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell =[[AnalysisDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    [((AnalysisDetailCell *)cell) initWithAnalysisModel:model index:indexPath.section];
    //配置视图显示
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

-(void)actionOpen:(UIGestureRecognizer *)singleTap
{
    NSInteger tag=singleTap.view.tag;
    AnalysisModel *model = [gameData objectAtIndex:tag];
    [self showIndex:tag detail:model.opened];
}

-(void)showIndex:(NSInteger)index detail:(BOOL)detail
{
    if (detail==NO) {
        AnalysisModel *model=[gameData objectAtIndex:index];
        model.opened=YES;
        [gameData replaceObjectAtIndex:index withObject:model];
        NSMutableArray *insertIndexPaths = [NSMutableArray arrayWithCapacity:1];
        NSIndexPath *newPath = [NSIndexPath indexPathForRow:0 inSection:index];
        [insertIndexPaths addObject:newPath];
        [gameTable beginUpdates];
        [gameTable reloadRowsAtIndexPaths:insertIndexPaths withRowAnimation:UITableViewRowAnimationFade];
        [gameTable endUpdates];
    }else{
        AnalysisModel *model=[gameData objectAtIndex:index];
        model.opened=NO;
        [gameData replaceObjectAtIndex:index withObject:model];
        NSMutableArray *insertIndexPaths = [NSMutableArray arrayWithCapacity:1];
        NSIndexPath *newPath = [NSIndexPath indexPathForRow:0 inSection:index];
        [insertIndexPaths addObject:newPath];
        [gameTable beginUpdates];
        [gameTable reloadRowsAtIndexPaths:insertIndexPaths withRowAnimation:UITableViewRowAnimationFade];
        [gameTable endUpdates];
    }
}

#pragma mark -- PullingRefreshTableViewDelegate
- (void)pullingTableViewDidStartRefreshing:(PullingRefreshTableView *)tableView
{
    [self requestInfoWithStart:0];
}

- (void)pullingTableViewDidStartLoading:(PullingRefreshTableView *)tableView
{
    [self requestInfoWithStart:gameData.count];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [gameTable tableViewDidScroll:scrollView];
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [gameTable tableViewDidEndDragging:scrollView];
}

-(void)requestInfoWithStart:(NSInteger)start
{
    NSString * urlString = [[NSString stringWithFormat:@"%@/manage/data/phone/list?start=%@&rows=%d",kHomeHost,@(start),20] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    DLog(@"url---%@",urlString);
    NSURL * url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLRequest *request=[NSURLRequest requestWithURL:url];
    [NetworkManager invokeRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON){
        if (start==0) {
            [gameData removeAllObjects];
        }
        for (NSDictionary *dic in [JSON objectForKey:@"data"]) {
            AnalysisModel *model=[[AnalysisModel alloc] initWithDic:dic];
            [gameData addObject:model];
        }
        [gameTable reloadData];
        [gameTable tableViewDidFinishedLoading];
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        
    }];
}

#pragma mark -- RNFrostedSidebarDelegate

- (void)sidebar:(RNFrostedSidebar *)sidebar didTapItemAtIndex:(NSUInteger)index {
    DLog(@"Tapped item at index %@",@(index));
    [sidebar dismiss];
}

- (void)sidebar:(RNFrostedSidebar *)sidebar didEnable:(BOOL)itemEnabled itemAtIndex:(NSUInteger)index {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
