//
//  main.m
//  bestgame
//
//  Created by DongboGuan on 15/6/27.
//  Copyright (c) 2015年 DongboGuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
