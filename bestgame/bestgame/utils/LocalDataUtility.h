//
//  CommonUtility.h
//  WPWProject
//
//  Created by 豪 on 13-11-21.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalDataUtility : NSObject

+(id)getValueForKey:(NSString *)key;
+(void)setValue:(id)obj forKey:(NSString *)key;

@end
