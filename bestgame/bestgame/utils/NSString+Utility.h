//
//  NSString+Utility.h
//  WPWProject
//
//  Created by Mr.Lu on 13-7-24.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utility)

-(BOOL)containStr:(NSString *)str;
//-(NSString *)unicode2emoji;//unicode码转换成带emoji的上传到服务端
//-(NSString *)emoji2unicode;//带emoji表情的转换成unicode码
-(BOOL)validateEmail;
-(BOOL)validatePhone;
-(int)getToInt;
-(NSString *)removeQuata;//删除单引号 本地数据库缓存

-(UIImage *)string2Image;

@end
