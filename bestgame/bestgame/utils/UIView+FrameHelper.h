//
//  UIView+FrameHelper.h
//  FilmWithYou
//
//  Created by 王 易平 on 9/12/14.
//  Copyright (c) 2014 王 易平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (FrameHelper)

/**
 *  @brief  Setter: 改变Size，左上角位置不变
 */
@property(assign, nonatomic)CGSize size;

@property(assign, nonatomic)CGFloat width;
@property(assign, nonatomic)CGFloat height;

@property(assign, nonatomic)CGPoint top;
@property(assign, nonatomic)CGPoint bottom;
@property(assign, nonatomic)CGPoint left;
@property(assign, nonatomic)CGPoint right;

@property(assign, nonatomic)CGPoint leftTop;
@property(assign, nonatomic)CGPoint rightTop;

@property(assign, nonatomic)CGPoint leftBottom;
@property(assign, nonatomic)CGPoint rightBottom;

@end
