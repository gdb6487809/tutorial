//
//  compress.m
//  WPWProject
//
//  Created by Mr.Lu on 13-7-16.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import "UIImage+Compress.h"
#define MAX_IMAGEPIX 160.0 // max pix 200.0px
#define MAX_IMAGEDATA_LEN 100000.0 // max data length 300k

@implementation UIImage (Compress)

-(UIImage *)compressedImageWithMaxSize:(CGSize)maxSize
{
    CGSize imageSize = self.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    
    if (width <= maxSize.width && height <= maxSize.height) {
        // no need to compress.
        return self;
    }
    
    if (width == 0 || height == 0) {
        // void zero exception
        return self;
    }
    
    UIImage *newImage = nil;
    CGFloat widthFactor = maxSize.width / width;
    CGFloat heightFactor = maxSize.height / height;
    CGFloat scaleFactor = 0.5;
    
    if (widthFactor > heightFactor)
        scaleFactor = heightFactor; // scale to fit height
    else
        scaleFactor = widthFactor; // scale to fit width
    
    CGFloat scaledWidth = width * scaleFactor;
    CGFloat scaledHeight = height * scaleFactor;
    CGSize targetSize = CGSizeMake(scaledWidth, scaledHeight);
    
    UIGraphicsBeginImageContext(targetSize); // this will crop
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.size.width = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [self drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    
    return newImage;
}


-(UIImage *)compressedImage {
    return [self compressedImageWithMaxSize:CGSizeMake(MAX_IMAGEPIX, MAX_IMAGEPIX)];
}

-(UIImage *)compressWithScale:(float)scale
{
    UIGraphicsBeginImageContext(CGSizeMake(self.size.width*scale,self.size.height*scale));
    [self drawInRect:CGRectMake(0, 0, self.size.width*scale, self.size.height *scale)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}


@end

