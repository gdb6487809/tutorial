//
//  NetworkManager.m
//  WPWProject
//
//  Created by 王 易平 on 9/10/14.
//  Copyright (c) 2014 Mr.Lu. All rights reserved.
//

#import "NetworkManager.h"
#import "AFNetworking.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "UIProgressView+AFNetworking.h"

@interface NetworkRequest : NSObject

@property (nonatomic, strong) AFHTTPRequestOperation *operation;
@property (nonatomic, strong) NSMutableArray *eventHandlerArray;

@end

@implementation NetworkRequest

@end

@interface EventStorage : NSObject

@property (nonatomic, assign) id<NetworkDelegate> eventHandler;
@property (nonatomic, assign) void * context;
@property (nonatomic, strong) void (^successBlock)(NSURLRequest *request, NSHTTPURLResponse *response, id JSON);
@property (nonatomic, strong) void (^failureBlock)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON);

@end

@implementation EventStorage

@end


@interface NetworkManager ()
{
    NSMutableArray *requestArray;
}

@end

@implementation NetworkManager

@synthesize requestArray = requestArray;

+(instancetype) sharedInstance
{
    static NetworkManager *_sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

-(id)init
{
    if (self = [super init]) {
        self.requestArray = [NSMutableArray array];
    }
    return self;
}


+(BOOL)invokeGetRequest:(NSString *)url parameters:(id)params eventHandler:(id<NetworkDelegate>)eventHandler context:(void *)context
{
    NetworkManager *manager = [NetworkManager sharedInstance];
    
    for (NetworkRequest *request in manager.requestArray) {
        
        AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
        NSError *error;
        NSMutableURLRequest * newRequest = [serializer requestWithMethod:@"GET" URLString:url parameters:params error:&error];
        if (error) {
            NSLog(@"%@", error);
            return NO;
        }
        
        AFHTTPRequestOperation *op = request.operation;
        if ([op.request.URL.absoluteString isEqualToString:newRequest.URL.absoluteString] &&
            [op.request.HTTPMethod isEqualToString:@"GET"]) {
            EventStorage *storage = [[EventStorage alloc] init];
            storage.eventHandler = eventHandler;
            storage.context = context;
            [request.eventHandlerArray addObject:storage];
            return YES;
        }
    }
    
    EventStorage *storage = [[EventStorage alloc] init];
    storage.eventHandler = eventHandler;
    storage.context = context;
    
    NetworkRequest *request = [[NetworkRequest alloc] init];
    request.eventHandlerArray = [[NSMutableArray alloc] init];
    [request.eventHandlerArray addObject:storage];
    
    [manager.requestArray addObject:request];
    
    request.operation = [[AFHTTPRequestOperationManager manager] GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [manager requestDidFinished:operation responseObject:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [manager requestDidFailed:operation error:error];
    }];
    
    [[AFNetworkActivityIndicatorManager sharedManager] incrementActivityCount];
    
    return YES;
}

+(BOOL) invokeGetRequest:(NSString *)requestString eventHandler:(id<NetworkDelegate>)eventHandler context:(void *)context
{
    
    NetworkManager *manager = [NetworkManager sharedInstance];
    
    for (NetworkRequest *request in manager.requestArray) {
        
        
        AFHTTPRequestOperation *op = request.operation;
        if ([op.request.URL.absoluteString isEqualToString:requestString] &&
            [op.request.HTTPMethod isEqualToString:@"GET"]) {
            EventStorage *storage = [[EventStorage alloc] init];
            storage.eventHandler = eventHandler;
            storage.context = context;
            [request.eventHandlerArray addObject:storage];
            return YES;
        }
    }
    
    EventStorage *storage = [[EventStorage alloc] init];
    storage.eventHandler = eventHandler;
    storage.context = context;
    
    NetworkRequest *request = [[NetworkRequest alloc] init];
    request.eventHandlerArray = [[NSMutableArray alloc] init];
    [request.eventHandlerArray addObject:storage];
    
    [manager.requestArray addObject:request];
    
    request.operation = [[AFHTTPRequestOperationManager manager] GET:requestString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [manager requestDidFinished:operation responseObject:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [manager requestDidFailed:operation error:error];
    }];
    
    [[AFNetworkActivityIndicatorManager sharedManager] incrementActivityCount];
    
    return YES;
}

+(BOOL)invokePostRequest:(NSString *)url parameters:(id)params eventHandler:(id<NetworkDelegate>)eventHandler context:(void *)context
{
    NetworkManager *manager = [NetworkManager sharedInstance];
    
    for (NetworkRequest *request in manager.requestArray) {
        
        AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
        
        NSError *error;
        NSMutableURLRequest *newRequest = [serializer requestWithMethod:@"POST" URLString:url parameters:params error:&error];
        if (error) {
            NSLog(@"%@", error);
            return NO;
        }
        
        AFHTTPRequestOperation *op = request.operation;
        NSString *opDataStr = [[NSString alloc] initWithData:op.request.HTTPBody encoding:NSUTF8StringEncoding];
        NSString *newReqDataStr = [[NSString alloc] initWithData:newRequest.HTTPBody encoding:NSUTF8StringEncoding];
        
        if ([op.request.URL.absoluteString isEqualToString:url] &&
            [op.request.HTTPMethod isEqualToString:@"POST"] &&
            [opDataStr isEqualToString:newReqDataStr]
            ) {
            EventStorage *storage = [[EventStorage alloc] init];
            storage.eventHandler = eventHandler;
            storage.context = context;
            [request.eventHandlerArray addObject:storage];
            return YES;
        }
    }
    
    EventStorage *storage = [[EventStorage alloc] init];
    storage.eventHandler = eventHandler;
    storage.context = context;
    
    NetworkRequest *request = [[NetworkRequest alloc] init];
    request.eventHandlerArray = [[NSMutableArray alloc] init];
    [request.eventHandlerArray addObject:storage];
    
    [manager.requestArray addObject:request];
    
    request.operation = [[AFHTTPRequestOperationManager manager] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [manager requestDidFinished:operation responseObject:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [manager requestDidFailed:operation error:error];
    }];
    
    [[AFNetworkActivityIndicatorManager sharedManager] incrementActivityCount];
    
    return YES;
}

+(BOOL)invokeGetRequest:(NSString *)url parameters:(id)params success:(void (^)(NSURLRequest *, NSHTTPURLResponse *, id))success failure:(void (^)(NSURLRequest *, NSHTTPURLResponse *, NSError *, id))failure
{
    NetworkManager *manager = [NetworkManager sharedInstance];
    
    for (NetworkRequest *request in manager.requestArray) {
        
        AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
        NSError *error;
        NSMutableURLRequest * newRequest = [serializer requestWithMethod:@"GET" URLString:url parameters:params error:&error];
        if (error) {
            NSLog(@"%@", error);
            return NO;
        }
        
        AFHTTPRequestOperation *op = request.operation;
        if ([op.request.URL.absoluteString isEqualToString:newRequest.URL.absoluteString] &&
            [op.request.HTTPMethod isEqualToString:@"GET"]) {
            EventStorage *storage = [[EventStorage alloc] init];
            storage.successBlock = success;
            storage.failureBlock = failure;
            [request.eventHandlerArray addObject:storage];
            return YES;
        }
    }
    
    EventStorage *storage = [[EventStorage alloc] init];
    storage.successBlock = success;
    storage.failureBlock = failure;
    
    NetworkRequest *request = [[NetworkRequest alloc] init];
    request.eventHandlerArray = [[NSMutableArray alloc] init];
    [request.eventHandlerArray addObject:storage];
    
    [manager.requestArray addObject:request];
    
    request.operation = [[AFHTTPRequestOperationManager manager] GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [manager requestDidFinished:operation responseObject:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [manager requestDidFailed:operation error:error];
    }];
    
    [[AFNetworkActivityIndicatorManager sharedManager] incrementActivityCount];
    
    return YES;
}

+(BOOL)invokeGetRequest:(NSString *)requestString success:(void (^)(NSURLRequest *, NSHTTPURLResponse *, id))success failure:(void (^)(NSURLRequest *, NSHTTPURLResponse *, NSError *, id))failure
{
    NetworkManager *manager = [NetworkManager sharedInstance];
    
    for (NetworkRequest *request in manager.requestArray) {
        
        
        AFHTTPRequestOperation *op = request.operation;
        if ([op.request.URL.absoluteString isEqualToString:requestString] &&
            [op.request.HTTPMethod isEqualToString:@"GET"]) {
            EventStorage *storage = [[EventStorage alloc] init];
            storage.successBlock = success;
            storage.failureBlock = failure;
            [request.eventHandlerArray addObject:storage];
            return YES;
        }
    }
    
    EventStorage *storage = [[EventStorage alloc] init];
    storage.successBlock = success;
    storage.failureBlock = failure;
    
    NetworkRequest *request = [[NetworkRequest alloc] init];
    request.eventHandlerArray = [[NSMutableArray alloc] init];
    [request.eventHandlerArray addObject:storage];
    
    [manager.requestArray addObject:request];
    
    request.operation = [[AFHTTPRequestOperationManager manager] GET:requestString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [manager requestDidFinished:operation responseObject:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [manager requestDidFailed:operation error:error];
    }];
    
    [[AFNetworkActivityIndicatorManager sharedManager] incrementActivityCount];
    
    return YES;
}

+(BOOL)invokePostRequest:(NSString *)url parameters:(id)params success:(void (^)(NSURLRequest *, NSHTTPURLResponse *, id))success failure:(void (^)(NSURLRequest *, NSHTTPURLResponse *, NSError *, id))failure
{
    NetworkManager *manager = [NetworkManager sharedInstance];
    
    for (NetworkRequest *request in manager.requestArray) {
        
        AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
        
        NSError *error;
        NSMutableURLRequest *newRequest = [serializer requestWithMethod:@"POST" URLString:url parameters:params error:&error];
        if (error) {
            NSLog(@"%@", error);
            return NO;
        }
        
        AFHTTPRequestOperation *op = request.operation;
        NSString *opDataStr = [[NSString alloc] initWithData:op.request.HTTPBody encoding:NSUTF8StringEncoding];
        NSString *newReqDataStr = [[NSString alloc] initWithData:newRequest.HTTPBody encoding:NSUTF8StringEncoding];
        
        if ([op.request.URL.absoluteString isEqualToString:url] &&
            [op.request.HTTPMethod isEqualToString:@"POST"] &&
            [opDataStr isEqualToString:newReqDataStr]
            ) {
            EventStorage *storage = [[EventStorage alloc] init];
            storage.failureBlock = failure;
            storage.successBlock = success;
            [request.eventHandlerArray addObject:storage];
            return YES;
        }
    }
    
    EventStorage *storage = [[EventStorage alloc] init];
    storage.successBlock = success;
    storage.failureBlock = failure;
    
    NetworkRequest *request = [[NetworkRequest alloc] init];
    request.eventHandlerArray = [[NSMutableArray alloc] init];
    [request.eventHandlerArray addObject:storage];
    
    [manager.requestArray addObject:request];
    
    request.operation = [[AFHTTPRequestOperationManager manager] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [manager requestDidFinished:operation responseObject:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [manager requestDidFailed:operation error:error];
    }];
    
    [[AFNetworkActivityIndicatorManager sharedManager] incrementActivityCount];
    
    return YES;
}

+(BOOL)invokeRequest:(NSURLRequest *)request success:(void (^)(NSURLRequest *, NSHTTPURLResponse *, id))success failure:(void (^)(NSURLRequest *, NSHTTPURLResponse *, NSError *, id))failure
{
    NetworkManager *manager = [NetworkManager sharedInstance];
    
    NSString *requestBody;
    if ([request.HTTPMethod isEqualToString:@"POST"]) {
        requestBody = [[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding];
    }
    
    
    NSString *method = request.HTTPMethod;
    
    for (NetworkRequest *netRequest in manager.requestArray) {
        if ([method isEqualToString:netRequest.operation.request.HTTPMethod]) {
            if ([method isEqualToString:@"GET"] &&
                [netRequest.operation.request.URL isEqual:request.URL]) {
                EventStorage *event = [[EventStorage alloc] init];
                event.successBlock = success;
                event.failureBlock = failure;
                [netRequest.eventHandlerArray addObject:event];
                return YES;
            }
            
            
            if ([method isEqualToString:@"POST"] &&
                [netRequest.operation.request.URL isEqual:request.URL] &&
                netRequest.operation) {
                NSString *opBody = [[NSString alloc] initWithData:netRequest.operation.request.HTTPBody encoding:NSUTF8StringEncoding];
                if ([opBody isEqualToString:requestBody]) {
                    EventStorage *event = [[EventStorage alloc] init];
                    event.successBlock = success;
                    event.failureBlock = failure;
                    [netRequest.eventHandlerArray addObject:event];
                    return YES;
                }
            }
        }
    }
    
    AFHTTPRequestOperationManager *opManager = [AFHTTPRequestOperationManager manager];
    opManager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
    AFHTTPRequestOperation *operation =
        [opManager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [manager requestDidFinished:operation responseObject:responseObject];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [manager requestDidFailed:operation error:error];
        }];
    
    EventStorage *event = [[EventStorage alloc] init];
    event.successBlock = success;
    event.failureBlock = failure;
    
    NetworkRequest *networkRequest = [[NetworkRequest alloc] init];
    networkRequest.operation = operation;
    networkRequest.eventHandlerArray = [NSMutableArray array];
    [networkRequest.eventHandlerArray addObject:event];
    [manager.requestArray addObject:networkRequest];
    
    [[AFNetworkActivityIndicatorManager sharedManager] incrementActivityCount];
    [operation start];
    
    return YES;
}

+(BOOL)invokeRequest:(NSURLRequest *)request eventHandler:(id<NetworkDelegate>)eventHandler context:(void *)context
{
    NetworkManager *manager = [NetworkManager sharedInstance];
    
    NSString *requestBody;
    if ([request.HTTPMethod isEqualToString:@"POST"]) {
        requestBody = [[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding];
    }
    
    
    NSString *method = request.HTTPMethod;
    
    for (NetworkRequest *netRequest in manager.requestArray) {
        if ([method isEqualToString:netRequest.operation.request.HTTPMethod]) {
            if ([method isEqualToString:@"GET"] &&
                [netRequest.operation.request.URL isEqual:request.URL]) {
                EventStorage *event = [[EventStorage alloc] init];
                event.eventHandler = eventHandler;
                event.context = context;
                [netRequest.eventHandlerArray addObject:event];
                return YES;
            }
            
            
            if ([method isEqualToString:@"POST"] &&
                [netRequest.operation.request.URL isEqual:request.URL] &&
                netRequest.operation) {
                NSString *opBody = [[NSString alloc] initWithData:netRequest.operation.request.HTTPBody encoding:NSUTF8StringEncoding];
                if ([opBody isEqualToString:requestBody]) {
                    EventStorage *event = [[EventStorage alloc] init];
                    event.eventHandler = eventHandler;
                    event.context = context;
                    [netRequest.eventHandlerArray addObject:event];
                    return YES;
                }
            }
        }
    }
    
    AFHTTPRequestOperationManager *opManager = [AFHTTPRequestOperationManager manager];
    opManager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
    AFHTTPRequestOperation *operation;
    
    EventStorage *event = [[EventStorage alloc] init];
    event.eventHandler = eventHandler;
    event.context = context;
    
    NetworkRequest *networkRequest = [[NetworkRequest alloc] init];
    networkRequest.operation = operation;
    networkRequest.eventHandlerArray = [NSMutableArray array];
    [networkRequest.eventHandlerArray addObject:event];
    
    [manager.requestArray addObject:networkRequest];
    
    operation =
    [opManager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [manager requestDidFinished:operation responseObject:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [manager requestDidFailed:operation error:error];
    }];
    
    [[AFNetworkActivityIndicatorManager sharedManager] incrementActivityCount];
    [operation start];
    
    return YES;
    
}

+(BOOL) invokePostRequest:(NSString *)url
               parameters:(id)params
constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                  success:(void (^)(NSURLRequest *, NSHTTPURLResponse *, id))success
                  failure:(void (^)(NSURLRequest *, NSHTTPURLResponse *, NSError *, id))failure
      uploadProgressBlock:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite))progressBlock
{
    NetworkManager *manager = [NetworkManager sharedInstance];
    
    for (NetworkRequest *request in manager.requestArray) {
        
        AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
        
        NSError *error;
        NSMutableURLRequest *newRequest = [serializer requestWithMethod:@"POST" URLString:url parameters:params error:&error];
        if (error) {
            NSLog(@"%@", error);
            return NO;
        }
        
        AFHTTPRequestOperation *op = request.operation;
        NSString *opDataStr = [[NSString alloc] initWithData:op.request.HTTPBody encoding:NSUTF8StringEncoding];
        NSString *newReqDataStr = [[NSString alloc] initWithData:newRequest.HTTPBody encoding:NSUTF8StringEncoding];
        
        if ([op.request.URL.absoluteString isEqualToString:url] &&
            [op.request.HTTPMethod isEqualToString:@"POST"] &&
            [opDataStr isEqualToString:newReqDataStr]
            ) {
            EventStorage *storage = [[EventStorage alloc] init];
            storage.failureBlock = failure;
            storage.successBlock = success;
            [request.eventHandlerArray addObject:storage];
            return YES;
        }
    }
    
    EventStorage *storage = [[EventStorage alloc] init];
    storage.successBlock = success;
    storage.failureBlock = failure;
    
    NetworkRequest *request = [[NetworkRequest alloc] init];
    request.eventHandlerArray = [[NSMutableArray alloc] init];
    [request.eventHandlerArray addObject:storage];
    
    [manager.requestArray addObject:request];
    
    UIProgressView *progress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    progress.width = kDeviceWidth;
    [[UIApplication sharedApplication].windows[0] addSubview:progress];
    
    request.operation = [[AFHTTPRequestOperationManager manager] POST:url parameters:params constructingBodyWithBlock:block success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [manager requestDidFinished:operation responseObject:responseObject];
        [progress removeFromSuperview];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [manager requestDidFailed:operation error:error];
        [progress removeFromSuperview];
    }];
    [request.operation setUploadProgressBlock:progressBlock];
    [progress setProgressWithUploadProgressOfOperation:request.operation animated:YES];
    
    [[AFNetworkActivityIndicatorManager sharedManager] incrementActivityCount];
    
    return YES;
}


-(void) requestDidFinished:(AFHTTPRequestOperation *)operation responseObject:(id)responseObject
{
    [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
    for (NetworkRequest *request in self.requestArray) {
        if ([NetworkManager compareRequest:operation.request withRequest:request.operation.request]) {
            for (EventStorage *event in request.eventHandlerArray) {
                if (event.eventHandler && [event.eventHandler respondsToSelector:@selector(requestDidFinished:responseObject:context:)]) {
                    [event.eventHandler requestDidFinished:operation responseObject:responseObject context:event.context];
                }
                if (event.successBlock) {
                    event.successBlock(operation.request, operation.response, responseObject);
                    event.successBlock = nil;
                    event.failureBlock = nil;
                }
                
            }
            [request.eventHandlerArray removeAllObjects];
            request.eventHandlerArray = nil;
            
//            NSLog(@"Finished: %@", request.operation.request.URL.absoluteString);
            
            request.operation = nil;
            
            [self.requestArray removeObject:request];
//            NSLog(@"%@", self.requestArray);
            
            break;
        }
    }
}


-(void) requestDidFailed:(AFHTTPRequestOperation *)operation error:(NSError *)error
{
    NSLog(@"Fail: %@", operation.request.URL.absoluteString);
    
    [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
    for (NetworkRequest *request in self.requestArray) {
        if ([NetworkManager compareRequest:operation.request withRequest:request.operation.request]) {
            for (EventStorage *event in request.eventHandlerArray) {
                if (event.eventHandler && [event.eventHandler respondsToSelector:@selector(requestDidFailed:error:context:)]) {
                    [event.eventHandler requestDidFailed:operation error:error context:event.context];
                }
                if (event.failureBlock) {
                    event.failureBlock(operation.request, operation.response, error, nil);
                    event.successBlock = nil;
                    event.failureBlock = nil;
                }
            }
            request.operation = nil;
            
            [self.requestArray removeObject:request];
            
            break;
        }
    }
}

+(BOOL) compareRequest:(NSURLRequest *)requestA withRequest:(NSURLRequest *)requestB
{
    if (![requestA.URL isEqual:requestB.URL]) {
        return NO;
    }
    if (![requestA.HTTPMethod isEqualToString:requestB.HTTPMethod]) {
        return NO;
    }
//    NSString *preBody = [[NSString alloc] initWithData:requestA.HTTPBody encoding:NSUTF8StringEncoding];
//    NSString *postBody = [[NSString alloc] initWithData:requestB.HTTPBody encoding:NSUTF8StringEncoding];
//    if (![preBody isEqualToString:postBody]) {
//        return NO;
//    }
    return YES;
}

+ (void)downloadFileURL:(NSString *)aUrl savePath:(NSString *)aSavePath fileName:(NSString *)aFileName
                success:(void (^)(NSString *, id))success
                failure:(void (^)(NSString *, id))failure
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //检查本地文件是否已存在
    NSString *fileName = [NSString stringWithFormat:@"%@/%@", aSavePath, aFileName];
    
    //检查附件是否存在
    if ([fileManager fileExistsAtPath:fileName]) {
//        NSData *audioData = [NSData dataWithContentsOfFile:fileName];
        success(fileName,nil);
    }else{
        //创建附件存储目录
        if (![fileManager fileExistsAtPath:aSavePath]) {
            [fileManager createDirectoryAtPath:aSavePath withIntermediateDirectories:YES attributes:nil error:nil];
        }
        
        //下载附件
        NSURL *url = [[NSURL alloc] initWithString:aUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        operation.inputStream   = [NSInputStream inputStreamWithURL:url];
        operation.outputStream  = [NSOutputStream outputStreamToFileAtPath:fileName append:NO];
        
        //下载进度控制
        /*
         [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
         NSLog(@"is download：%f", (float)totalBytesRead/totalBytesExpectedToRead);
         }];
         */
        
        //已完成下载
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
//            NSData *audioData = [NSData dataWithContentsOfFile:fileName];
            //设置下载数据到res字典对象中并用代理返回下载数据NSData
            success(fileName,responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            //下载失败
            failure(nil,nil);
        }];
        
        [operation start];
    }
}

@end
