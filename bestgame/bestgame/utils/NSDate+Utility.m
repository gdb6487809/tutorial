//
//  NSDate+Utility.m
//  WPWProject
//
//  Created by Mr.Lu on 13-7-25.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import "NSDate+Utility.h"  
#import "SingletonUtility.h"

@implementation NSDate (Utility)


-(NSString *)formateDateWithFormate:(NSString *)string
{
    NSDateFormatter *format=[SingletonUtility instance].formatter;
    //    [format setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [format setDateFormat:string];
    NSString * dateString = [[format stringFromDate:self]copy];
    
    return  dateString;
}

-(int)getDayDiffFromDate:(long long)fromDate
{
    NSDateFormatter *dateFormatter = [SingletonUtility instance].formatter;
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *from = [NSDate dateWithTimeIntervalSince1970:fromDate];
    NSDate *to = self;
    
    NSString *fromStr=[dateFormatter stringFromDate:from];
    NSString *toStr=[dateFormatter stringFromDate:to];
    
    NSTimeInterval diff=[[dateFormatter dateFromString:toStr] timeIntervalSinceDate:[dateFormatter dateFromString:fromStr]];
    int days=(int)(diff/(60*60*24));
    return days;
}

-(int)getHourDiffFromDate:(long long)fromDate
{
    NSDateFormatter *dateFormatter = [SingletonUtility instance].formatter;
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH"];
    
    NSDate *from = [NSDate dateWithTimeIntervalSince1970:fromDate];
    NSDate *to = self;
    
    NSString *fromStr=[dateFormatter stringFromDate:from];
    NSString *toStr=[dateFormatter stringFromDate:to];
    
    NSTimeInterval diff=[[dateFormatter dateFromString:toStr] timeIntervalSinceDate:[dateFormatter dateFromString:fromStr]];
    int hour=(int)(diff/(60*60));
    return hour;
}

-(int)getMinuteDiffFromDate:(long long)fromDate
{
    NSDateFormatter *dateFormatter = [SingletonUtility instance].formatter;
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSDate *from = [NSDate dateWithTimeIntervalSince1970:fromDate];
    NSDate *to = self;
    
    NSString *fromStr=[dateFormatter stringFromDate:from];
    NSString *toStr=[dateFormatter stringFromDate:to];
    
    NSTimeInterval diff=[[dateFormatter dateFromString:toStr] timeIntervalSinceDate:[dateFormatter dateFromString:fromStr]];
    int minute=(int)(diff/(60));
    return minute;
}
-(int)getSecondDiffFromDate:(long long)fromDate
{
    NSDateFormatter *dateFormatter = [SingletonUtility instance].formatter;
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *from = [NSDate dateWithTimeIntervalSince1970:fromDate];
    NSDate *to = self;
    
    NSString *fromStr=[dateFormatter stringFromDate:from];
    NSString *toStr=[dateFormatter stringFromDate:to];
    
    NSTimeInterval diff=[[dateFormatter dateFromString:toStr] timeIntervalSinceDate:[dateFormatter dateFromString:fromStr]];
    int second=(int)(diff);
    if (second<0) {
        second=0;
    }
    return second;
}

+(NSString *)timeStamp:(long long)date
{
    NSString *timestamp = @"";
    
    NSDateComponents *components=[self getCompareWithDate:date];
    if (components.day>=7) {
        timestamp = @"一周前";
    }else if(components.day>0){
        timestamp = [NSString stringWithFormat:@"%@%@", @(components.day), @"天前"];
    }else if(components.hour>0){
        timestamp = [NSString stringWithFormat:@"%@%@", @(components.hour), @"小时前"];
    }else if(components.minute>0){
        timestamp = [NSString stringWithFormat:@"%@%@", @(components.minute), @"分钟前"];
    }else {
        timestamp = [NSString stringWithFormat:@"%@%@", @(components.second), @"秒前"];
    }
    return timestamp;
    
}

+(NSDateComponents *)getCompareWithDate:(long long)date
{
    NSDateComponents *components=[[NSDateComponents alloc] init];
    components.day=[[NSDate new] getDayDiffFromDate:date];
    components.hour=[[NSDate new] getHourDiffFromDate:date];
    components.minute=[[NSDate new] getMinuteDiffFromDate:date];
    components.second=[[NSDate new] getSecondDiffFromDate:date];
    return components;
}


@end
