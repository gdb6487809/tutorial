//
//  CommonUtility.m
//  WPWProject
//
//  Created by 豪 on 13-11-21.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import "EmptyUtility.h"
#import <AVFoundation/AVFoundation.h>

@implementation EmptyUtility

+(BOOL)isEmpty:(NSObject *)obj
{
    if (obj==nil) {
        return YES;
    }
    if ([@"" isEqualToString:(NSString *)obj]||[obj isKindOfClass:[NSNull class]]||[@"(null)" isEqualToString:(NSString *)obj]||[@"<null>" isEqualToString:(NSString *)obj]||[@"<nil>" isEqualToString:(NSString *)obj]) {
        return YES;
    }else{
        if ([obj isKindOfClass:[NSString class]]&&[[(NSString *)obj stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0) {
            return YES;
        }else{
            return NO;
        }
    }
}
@end
