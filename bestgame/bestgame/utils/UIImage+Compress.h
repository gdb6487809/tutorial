//
//  compress.h
//  WPWProject
//
//  Created by Mr.Lu on 13-7-16.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Compress)

-(UIImage *)compressedImage;

-(UIImage *)compressedImageWithMaxSize:(CGSize) maxSize;

-(UIImage *)compressWithScale:(float)scale;

@end



