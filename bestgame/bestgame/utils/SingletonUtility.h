//
//  SingletonUtility.h
//  WPWProject
//
//  Created by 关 东波 on 14-2-20.
//  Copyright (c) 2014年 Mr.Lu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "CustomNaviViewController.h"
#import "AppDelegate.h"

@interface SingletonUtility : NSObject
{
    
}

@property(nonatomic,strong)NSDateFormatter *formatter;
@property(nonatomic,strong)AppDelegate *appDelegate;

+(SingletonUtility *)instance;

@end
