//
//  NSString+Utility.m
//  WPWProject
//
//  Created by Mr.Lu on 13-7-24.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import "NSString+Utility.h"

@implementation NSString (Utility)

-(BOOL)validateEmail
{
    NSString *codeRegex = @"^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";
    NSPredicate *nameCodeTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", codeRegex];
    return [nameCodeTest evaluateWithObject:self];
}

-(BOOL)validatePhone
{
    NSString *codeRegex = @"[0-9]{11}";
    NSPredicate *phoneCodeTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", codeRegex];
    return [phoneCodeTest evaluateWithObject:self];
}
-(BOOL)containStr:(NSString *)str
{
    NSRange range = [self rangeOfString:str];
    //如果range的location属性为NSNotFound则为没有找到   //(NSNotFound)最大的数值
    if(range.location == NSNotFound){
        return NO;
    }else{
        return YES;
    }
}

//计算字节长度
- (int)getToInt
{
//    NSData* testData = [self dataUsingEncoding:NSUTF8StringEncoding];
//    int length =[testData length];
//    for(int i=0; i< [self length];i++){     //如果是汉字减去一字节
//        int a = [self characterAtIndex:i];
//        if( a > 0x4e00 && a < 0x9fff){
//            length--;
//        }
//    }
//    return length;
    int strlength = 0;
    char* p = (char*)[self cStringUsingEncoding:NSUnicodeStringEncoding];
    for (int i=0 ; i<[self lengthOfBytesUsingEncoding:NSUnicodeStringEncoding] ;i++) {
        if (*p) {
            p++;
            strlength++;
        }else {
            p++;
        }
    }
    return (strlength+1)/2;
}

-(NSString *)removeQuata
{
    if ([self containStr:@"'"]) {
        return [self stringByReplacingOccurrencesOfString:@"'" withString:@"’"];
    }
    return self;
}

-(UIImage *)string2Image  //字符串下默认创建一个透明的位图
{
    UIFont* font = [UIFont systemFontOfSize:18.0f];
    CGRect frame=[self boundingRectWithSize:CGSizeMake(kDeviceWidth-20, 90000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    CGSize size = frame.size;
    // Create a bitmap context into which the text will be rendered.
    UIGraphicsBeginImageContext(CGSizeMake(size.width<kDeviceWidth?kDeviceWidth:size.width, size.height<100?100:(size.height+50)));
    // Render the text
//    [self drawInRect:CGRectMake(10, 10, size.width, size.height) withFont:font];
    [self drawInRect:frame withAttributes:@{NSFontAttributeName:font}];
    // Retrieve the image
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
