//
//  IntroductionView.m
//  WPWProject
//
//  Created by 关 东波 on 13-12-20.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import "IntroductionView.h"

@implementation IntroductionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)showIntroduction:(NSString *)sourceFolder
{
    NSFileManager *fileManager=[NSFileManager defaultManager];
    NSArray *fileList=[fileManager contentsOfDirectoryAtPath:sourceFolder error:nil];
    
    imageList=[[NSMutableArray alloc] init];
    for (NSString *file in fileList) {
        NSString *path = [sourceFolder stringByAppendingPathComponent:file];
        [imageList addObject:path];
    }
    if (imageList.count>0) {
        [self appearImageIndex:0];
    }
}

-(void)appearImageIndex:(NSInteger)index
{
    if (index<imageList.count) {
        UIImageView *introImageView=[[UIImageView alloc] initWithFrame:self.bounds];
        introImageView.tag=index;
        introImageView.userInteractionEnabled=YES;
        introImageView.image=[UIImage imageWithContentsOfFile:[imageList objectAtIndex:index]];
        [introImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapIntroductionView:)]];
        [self addSubview:introImageView];
    }else{
        if ([self.delegate respondsToSelector:@selector(introductionEnd)]) {
            [self.delegate introductionEnd];
        }
        [self removeFromSuperview];
    }
}

-(void)tapIntroductionView:(UIGestureRecognizer *)ges
{
    NSInteger tag=ges.view.tag;
    [ges.view removeFromSuperview];
    [self appearImageIndex:++tag];
}

@end
