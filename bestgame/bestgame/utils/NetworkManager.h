//
//  NetworkManager.h
//  WPWProject
//
//  Created by 王 易平 on 9/10/14.
//  Copyright (c) 2014 Mr.Lu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@protocol NetworkDelegate <NSObject>

@optional
/**
 *  @brief  网络请求成功的回调方法
 *
 *  @param operation
 *  @param responseObject 服务器传回的JSON，解析为NSDictionary/NSArray
 *  @param context        唯一标示，用于回调判断
 */
-(void) requestDidFinished:(AFHTTPRequestOperation *)operation responseObject:(id)responseObject context:(void *)context;

/**
 *  @brief  网络请求失败回调方法
 *
 *  @param operation
 *  @param error     错误信息
 *  @param context   唯一标示，用于回调判断
 */
-(void) requestDidFailed:(AFHTTPRequestOperation *)operation error:(NSError *)error context:(void *)context;

@end

@interface NetworkManager : NSObject

@property (nonatomic, strong) NSMutableArray *requestArray;

+(instancetype) sharedInstance;

/**
 *  @brief  静态Get请求，代理风格
 *
 *  @param requestString Http Get请求绝对地址
 *  @param eventHandler  代理回调对象
 *  @param context       唯一标示，用于回调判断
 *
 *  @return 请求构造结果
 */
+(BOOL) invokeGetRequest:(NSString *)requestString
            eventHandler:(id<NetworkDelegate>)eventHandler
                 context:(void *)context;

/**
 *  @brief  静态Get请求，代理风格
 *
 *  @param url          Http Get请求路径
 *  @param params       Http Get请求参数
 *  @param eventHandler 代理回调对象
 *  @param context      唯一标示，用于回调判断
 *
 *  @return 请求构造结果
 */
+(BOOL) invokeGetRequest:(NSString *)url
              parameters:(id)params
            eventHandler:(id<NetworkDelegate>)eventHandler
                 context:(void *)context;

/**
 *  @brief  静态Post请求，代理风格
 *
 *  @param url          Http Post请求路径
 *  @param params       Http Post请求参数，传入NSArray/NSDictionary，将构造为JSON
 *  @param eventHandler 代理回调对象
 *  @param context      唯一标示，用于回调判断
 *
 *  @return 请求构造结果
 */
+(BOOL) invokePostRequest:(NSString *)url
               parameters:(id)params
             eventHandler:(id<NetworkDelegate>)eventHandler
                  context:(void *)context;

/**
 *  @brief  静态Get请求，block风格
 *
 *  @param requestString Http Get请求绝对地址
 *  @param success       请求成功回调函数
 *  @param failure       请求失败回调函数
 *
 *  @return 请求构造结果
 */
+(BOOL) invokeGetRequest:(NSString *)requestString
                 success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, id JSON))success
                 failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON))failure;

/**
 *  @brief  静态Get请求，block风格
 *
 *  @param url     Http Get请求路径
 *  @param params  Http Get请求参数
 *  @param success 成功回调函数
 *  @param failure 失败回调函数
 *
 *  @return 请求构造结果
 */
+(BOOL) invokeGetRequest:(NSString *)url parameters:(id)params
                 success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, id JSON))success
                 failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON))failure;

/**
 *  @brief  静态Post请求，JSON编码，block风格
 *
 *  @param url     Http Post请求路径
 *  @param params  Http Post请求参数，接受NSDictionary/NSArray，将编码为JSON
 *  @param success 成功回调函数
 *  @param failure 失败回调函数
 *
 *  @return 请求构造结果
 */
+(BOOL) invokePostRequest:(NSString *)url parameters:(id)params
                 success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, id JSON))success
                 failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON))failure;

/**
 *  @brief  静态Post请求，FormData编码，block风格
 *
 *  @param url           Http Post请求路径
 *  @param params        Http Post请求参数
 *  @param block         FormData构造函数
 *  @param success       请求成功回调函数
 *  @param failure       请求失败回调函数
 *  @param progressBlock 上传进度回调函数
 *
 *  @return 请求构造结果
 */
+(BOOL) invokePostRequest:(NSString *)url
                parameters:(id)params
 constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                   success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, id JSON))success
                   failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON))failure
       uploadProgressBlock:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite))progressBlock;

/**
 *  @brief  静态启动一个NSURLRequest，block风格
 *
 *  @param request
 *  @param success 成功回调函数
 *  @param failure 失败回调函数
 *
 *  @return 请求构造结果
 */
+(BOOL) invokeRequest:(NSURLRequest *)request
              success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, id JSON))success
              failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON))failure;

/**
 *  @brief  静态启动一个NSURLRequest，代理风格
 *
 *  @param request
 *  @param eventHandler 代理回调对象
 *  @param context      唯一标示，用于回调判断
 *
 *  @return 请求构造结果
 */
+(BOOL) invokeRequest:(NSURLRequest *)request
         eventHandler:(id<NetworkDelegate>) eventHandler
              context:(void *)context;
//下载文件
+ (void)downloadFileURL:(NSString *)aUrl savePath:(NSString *)aSavePath fileName:(NSString *)aFileName
                success:(void (^)(NSString *, id))success
                failure:(void (^)(NSString *, id))failure;
@end
