//
//  UIView+Utility.h
//  WPWProject
//
//  Created by 关 东波 on 13-11-20.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Utility)

-(void)showTips:(NSString *)tips;

-(void)showLoadingViewWithTips:(NSString *)tips;
-(void)showLoadingViewWithTips:(NSString *)tips offsetY:(float)offsetY;
-(void)dismissLoadingView;

- (void)animatedIn;
- (void)animatedInWithScope:(float)scope;
- (void)animatedOut;
- (void)animateOutWithoutRemove;
- (void)shakeWithDuration:(float)duration;

-(void)showIntroduction;

@end
