//
//  CommonUtility.m
//  WPWProject
//
//  Created by 豪 on 13-11-21.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import "LocalDataUtility.h"
#import <AVFoundation/AVFoundation.h>

@implementation LocalDataUtility

+(id)getValueForKey:(NSString *)key
{
    id obj=[[NSUserDefaults standardUserDefaults] objectForKey:key];
    return obj;
}

+(void)setValue:(id)obj forKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setValue:obj forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
