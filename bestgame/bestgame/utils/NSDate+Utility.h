//
//  NSDate+Utility.h
//  WPWProject
//
//  Created by Mr.Lu on 13-7-25.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utility)

-(NSString *)formateDateWithFormate:(NSString *)string;
-(int)getDayDiffFromDate:(long long)fromDate;
-(int)getHourDiffFromDate:(long long)fromDate;
-(int)getMinuteDiffFromDate:(long long)fromDate;
-(int)getSecondDiffFromDate:(long long)fromDate;

@end
