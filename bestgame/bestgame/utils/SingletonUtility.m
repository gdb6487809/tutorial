//
//  SingletonUtility.m
//  WPWProject
//
//  Created by 关 东波 on 14-2-20.
//  Copyright (c) 2014年 Mr.Lu. All rights reserved.
//

#import "SingletonUtility.h"

@implementation SingletonUtility
@synthesize formatter;
@synthesize appDelegate;

static SingletonUtility *singletonUtility;

+(SingletonUtility *)instance
{
    @synchronized (self)
    {
        if (singletonUtility == nil)
        {
            singletonUtility = [[self alloc] init];
        }
    }
    
    return singletonUtility;
}

- (id)init
{
    if (self = [super init]) {
        formatter=[[NSDateFormatter alloc] init];
        appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return self;
}

@end
