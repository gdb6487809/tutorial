//
//  UIView+Utility.m
//  WPWProject
//
//  Created by 关 东波 on 13-11-20.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import "UIView+Utility.h"
#import "MBProgressHUD.h"
#import "SingletonUtility.h"
#import "IntroductionView.h"

#define LOADINGVIEW_TAG 84732
#define LOADINGVIEW_CIRCLE1 23898
#define LOADINGVIEW_CIRCLE2 46222

#define APPENDING_PATH_BY_BUNDLE(path) [[[NSBundle mainBundle]bundlePath]stringByAppendingPathComponent:path]

#define INTRODUCTION_IMAGE_FOLDER (iPhone5?APPENDING_PATH_BY_BUNDLE(@"introduction"):APPENDING_PATH_BY_BUNDLE(@"introductionSmall"))//首页引导页地址


@implementation UIView (Utility)

-(void)showTips:(NSString *)tips
{
    if (![self viewWithTag:494848]) {
        MBProgressHUD *hud=[[MBProgressHUD alloc] initWithView:self];
        hud.yOffset=-60;
        hud.labelText=tips;
        hud.labelFont=[UIFont systemFontOfSize:13];
        hud.removeFromSuperViewOnHide=YES;
        hud.tag=494848;
        hud.customView=[[UIView alloc] initWithFrame:CGRectZero];
        hud.mode=MBProgressHUDModeCustomView;
        hud.userInteractionEnabled=NO;
        [self addSubview:hud];
        [hud animatedIn];
        [hud show:YES];
        [hud hide:YES afterDelay:2.5];
    }
}
-(void)showLoadingViewWithTips:(NSString *)tips
{
    [self showLoadingViewWithTips:tips offsetY:30];
}

-(void)showLoadingViewWithTips:(NSString *)tips offsetY:(float)offsetY
{
    [self dismissLoadingView];
    if (![self viewWithTag:LOADINGVIEW_TAG]) {
        UIView *loadingBg=[[UIView alloc] initWithFrame:self.bounds];
        loadingBg.userInteractionEnabled=YES;
        loadingBg.tag=LOADINGVIEW_TAG;
        loadingBg.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"half_alpha_bg"]];
        
        UIImage *loadingBgImage=[UIImage imageNamed:@"loading_tips_bg"];
        UIImageView *logodingView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, loadingBgImage.size.width, loadingBgImage.size.height)];
        logodingView.center=CGPointMake(CGRectGetWidth(loadingBg.frame)/2, CGRectGetHeight(loadingBg.frame)/2-offsetY);
        logodingView.image = loadingBgImage;
        [loadingBg addSubview:logodingView];
        
        UIImage *logoImage=[UIImage imageNamed:@"loading_circle"];
        UIImageView *logoView=[[UIImageView alloc] initWithImage:logoImage];
        logoView.frame=CGRectMake(30, 0, logoImage.size.width, logoImage.size.height);
        logoView.tag=LOADINGVIEW_CIRCLE1;
        logoView.center=CGPointMake(logoView.center.x, CGRectGetHeight(logodingView.frame)/2);
        [logodingView addSubview:logoView];
        
        CABasicAnimation* rotationAnimation1;
        rotationAnimation1 = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        rotationAnimation1.toValue = [NSNumber numberWithFloat: M_PI * 2.0 ];
        
        rotationAnimation1.duration = 1.0;
        rotationAnimation1.cumulative = YES;
        rotationAnimation1.repeatCount = 9999999;
        [logoView.layer addAnimation:rotationAnimation1 forKey:@"rotationAnimation1"];
        
        [self addSubview:loadingBg];
        
        UILabel *tipsLab=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(logoView.frame)+25, 0, CGRectGetWidth(logodingView.frame)-CGRectGetMaxX(logoView.frame)-10, CGRectGetHeight(logodingView.frame))];
        tipsLab.font=[UIFont systemFontOfSize:14];
        tipsLab.backgroundColor=[UIColor clearColor];
        tipsLab.textColor=[UIColor colorWithRed:90/255.0 green:90/255.0 blue:90/255.0 alpha:1];;
        tipsLab.numberOfLines=0;
        //        tipsLab.textAlignment=NSTextAlignmentCenter;
        tipsLab.text=tips;
        tipsLab.center=CGPointMake(tipsLab.center.x, CGRectGetHeight(logodingView.frame)/2);
        [logodingView addSubview:tipsLab];
    }
}

-(void)dismissLoadingView
{
    if ([self viewWithTag:LOADINGVIEW_TAG]) {
        UIView *bgView=[self viewWithTag:LOADINGVIEW_TAG];
        UIView *circleView1=[bgView viewWithTag:LOADINGVIEW_CIRCLE1];
        [circleView1.layer removeAllAnimations];
        [bgView removeFromSuperview];
        bgView = nil;
    }
}

#pragma mark - Animated Mthod
- (void)animatedIn
{
    self.transform = CGAffineTransformMakeScale(1.4, 1.4);
    self.alpha = 1;
    [UIView animateWithDuration:.35 animations:^{
        self.alpha = 1;
        self.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)animatedOut
{
    [UIView animateWithDuration:.35 animations:^{
        self.transform = CGAffineTransformMakeScale(0.8, 0.8);
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        self.alpha = 1.0;
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

-(void)animateOutWithoutRemove
{
    self.transform = CGAffineTransformMakeScale(1.2, 1.2);
    self.alpha = 0.5;
    [UIView animateWithDuration:.35 animations:^{
        self.transform = CGAffineTransformMakeScale(0.8, 0.8);
        self.alpha = 1.0;
    } completion:^(BOOL finished) {
        self.transform = CGAffineTransformMakeScale(1, 1);
        self.alpha = 1.0;
    }];
}

- (void)animatedInWithScope:(float)scope
{
    self.transform = CGAffineTransformMakeScale(scope, scope);
    self.alpha = 0;
    [UIView animateWithDuration:.35 animations:^{
        self.alpha = 0.4;
        self.transform = CGAffineTransformMakeScale(1, 1);
    } completion:^(BOOL finished) {
        self.alpha = 1.0;
    }];
}

- (void)shakeWithDuration:(float)duration
{
    [self shakeTimes:8 currentTimes:0 duration:duration];
}

-(void)shakeTimes:(int)times currentTimes:(int)currentTimes duration:(float)duration
{
    [UIView animateWithDuration:duration animations:^{
		self.transform =  CGAffineTransformMakeTranslation(1.2, 0);
	} completion:^(BOOL finished) {
		if(currentTimes >= times) {
			self.transform = CGAffineTransformIdentity;
			return;
		}
		[self shakeTimes:times currentTimes:currentTimes+1 duration:duration];
	}];
}

#pragma mark - 显示引导页
-(void)showIntroduction
{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"first_introduction"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"first_introduction"];
        IntroductionView *introView = [[IntroductionView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight+20)];
        [[SingletonUtility instance].appDelegate.window addSubview:introView];
        [introView showIntroduction:INTRODUCTION_IMAGE_FOLDER];
    }
}



@end
