//
//  UIView+FrameHelper.m
//  FilmWithYou
//
//  Created by 王 易平 on 9/12/14.
//  Copyright (c) 2014 王 易平. All rights reserved.
//

#import "UIView+FrameHelper.h"

@implementation UIView (FrameHelper)

- (CGSize)size {
    
    return self.frame.size;
}

- (void)setSize:(CGSize)size {
    
    CGPoint origin = self.frame.origin;
    
    [self setFrame:CGRectMake(origin.x, origin.y, size.width, size.height)];
}

- (CGFloat)width {
    
    return CGRectGetWidth(self.frame);
}

- (void)setWidth:(CGFloat)width {
    
    [self setSize:CGSizeMake(width, self.height)];
}

- (CGFloat)height {
    
    return CGRectGetHeight(self.frame);
}

- (void)setHeight:(CGFloat)height {
    
    [self setSize:CGSizeMake(self.width, height)];
}

//Getters

- (CGPoint)top {
    
    return CGPointMake(self.center.x, self.center.y-self.height/2);
}

- (CGPoint)bottom {
    
    return CGPointMake(self.center.x, self.center.y+self.height/2);
}

- (CGPoint)left {
    
    return CGPointMake(self.center.x-self.width/2, self.center.y);
}

- (CGPoint)right {
    
    return CGPointMake(self.center.x+self.width/2, self.center.y);
}

//Setters

- (void)setTop:(CGPoint)top {
    
    [self setCenter:CGPointMake(top.x, top.y+self.height/2)];
}

- (void)setBottom:(CGPoint)bottom {
    
    [self setCenter:CGPointMake(bottom.x, bottom.y-self.height/2)];
}

- (void)setLeft:(CGPoint)left {
    
    [self setCenter:CGPointMake(left.x+self.width/2, left.y)];
}

- (void)setRight:(CGPoint)right {
    
    [self setCenter:CGPointMake(right.x-self.width/2, right.y)];
}

//Getters

- (CGPoint)leftTop {
    
    return CGPointMake(self.center.x-self.width/2, self.center.y-self.height/2);
}

- (CGPoint)rightTop {
    
    return CGPointMake(self.center.x+self.width/2, self.center.y-self.height/2);
}

- (CGPoint)leftBottom {
    
    return CGPointMake(self.center.x-self.width/2, self.center.y+self.height/2);
}

- (CGPoint)rightBottom {
    
    return CGPointMake(self.center.x+self.width/2, self.center.y+self.height/2);
}

//Setters

- (void)setLeftTop:(CGPoint)leftTop {
    
    [self setCenter:CGPointMake(leftTop.x+self.width/2, leftTop.y+self.height/2)];
}

- (void)setRightTop:(CGPoint)rightTop {
    
    [self setCenter:CGPointMake(rightTop.x-self.width/2, rightTop.y+self.height/2)];
}

- (void)setLeftBottom:(CGPoint)leftBottom {
    
    [self setCenter:CGPointMake(leftBottom.x+self.width/2, leftBottom.y-self.height/2)];
}

- (void)setRightBottom:(CGPoint)rightBottom {
    
    [self setCenter:CGPointMake(rightBottom.x-self.width/2, rightBottom.y-self.height/2)];
}

@end
