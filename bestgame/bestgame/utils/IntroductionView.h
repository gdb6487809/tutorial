//
//  IntroductionView.h
//  WPWProject
//
//  Created by 关 东波 on 13-12-20.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IntroductionViewDelegate <NSObject>

-(void)introductionEnd;

@end

@interface IntroductionView : UIView
{
    NSMutableArray *imageList;
}

-(void)showIntroduction:(NSString *)sourceFolder;
@property(nonatomic,assign) id<IntroductionViewDelegate> delegate;

@end
