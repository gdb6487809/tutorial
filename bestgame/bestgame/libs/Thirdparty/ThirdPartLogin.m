//
//  ThirdPartLogin.m
//  navi
//
//  Created by 关 东波 on 13-11-14.
//  Copyright (c) 2013年 关 东波. All rights reserved.
//

#import "ThirdPartLogin.h"
#import "JSONKit.h"
#import "NSDictionary+IsExitKey.h"

@interface ThirdPartLogin ()
{
    
}
@end

@implementation ThirdPartLogin
@synthesize qqUserInfo;
@synthesize sinaUserInfo;
@synthesize qqOAuth;

static ThirdPartLogin *thirdPartLogin;

+ (ThirdPartLogin *)instance
{
    @synchronized (self)
    {
        if (thirdPartLogin == nil)
        {
            thirdPartLogin = [[self alloc] init];
        }
    }
    
    return thirdPartLogin;
}

-(id)init
{
    self = [super init];
    if (self) {
        @try{
            qqOAuth = [[TencentOAuth alloc] initWithAppId:QQ_APP_ID andDelegate:self];
            
        }@catch(NSException *e){
            DLog(@"exception---------%@",e);
        }
    }
    return self;
}

#pragma mark sinaWeibo Login

- (void)loginSina
{
    WBAuthorizeRequest *request = [WBAuthorizeRequest request];
    request.redirectURI = @"https://api.weibo.com/oauth2/default.html";
    request.scope = @"all";
    request.userInfo = @{@"SSO_From": @"ThirdPartLoadViewController",
                         @"Other_Info_1": [NSNumber numberWithInt:123],
                         @"Other_Info_2": @[@"obj1", @"obj2"],
                         @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
    sinaRequestUrl=SINA_AUTH;
    [WeiboSDK sendRequest:request];
}

- (void)didReceiveWeiboRequest:(WBBaseRequest *)request
{
    if ([request isKindOfClass:WBProvideMessageForWeiboRequest.class])
    {
        
    }
}

- (void)didReceiveWeiboResponse:(WBBaseResponse *)response //授权之后
{
    if ([response isKindOfClass:WBSendMessageToWeiboResponse.class])
    {
        NSLog(@"发送结果---");
    }
    else if ([response isKindOfClass:WBAuthorizeResponse.class])
    {
//        NSString *wbtoken = [(WBAuthorizeResponse *)response userID];
        [LocalDataUtility setValue:[response.userInfo objectForKey:@"access_token"] forKey:@"sinaAccessToken"];
        [LocalDataUtility setValue:[response.userInfo objectForKey:@"uid"] forKey:@"sinaId"];
        
        DLog(@"%@-----userInfo",response.userInfo);
        if ([EmptyUtility isEmpty:[response.userInfo objectForKey:@"user_cancelled"]]) {
            if ([response.userInfo isExitKey:@"expires_in"]) {
                if ([[response.userInfo objectForKey:@"expires_in"] integerValue]<=0) {
                    
                }else{
                    [self getSinaUserInfo];
                }
            }else{
                
            }
        }else{
            
        }
    }
}

-(void)validateSinaExpire
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"json",@"format",SINA_APP_ID, @"oauth_consumer_key",[LocalDataUtility getValueForKey:@"sinaAccessToken"], @"access_token",nil];
    NSString *accessToken=[LocalDataUtility getValueForKey:@"sinaAccessToken"];
    [WBHttpRequest requestWithAccessToken:accessToken url:@"https://api.weibo.com/oauth2/get_token_info" httpMethod:@"POST" params:params delegate:self withTag:SINA_EXPIRE];
}

#pragma mark request SinaUserInfo
-(void)getSinaUserInfo
{
    NSDictionary *params=[NSDictionary dictionaryWithObjectsAndKeys:[LocalDataUtility getValueForKey:@"sinaId"],@"uid",SINA_APP_ID,@"source", nil];
//    DLog(@"%@--------params",params);
    NSString *accessToken=[LocalDataUtility getValueForKey:@"sinaAccessToken"];
    [WBHttpRequest requestWithAccessToken:accessToken url:@"https://api.weibo.com/2/users/show.json" httpMethod:@"GET" params:params delegate:self withTag:SINA_USERINFO];//7825693
}
- (void)request:(WBHttpRequest *)request didReceiveResponse:(NSURLResponse *)response
{

}

- (void)request:(WBHttpRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"error----%@",error);
}
//头像：avatar_url    昵称：nick_name    生日：birthday     性别：gender      签名：sign
- (void)request:(WBHttpRequest *)request didFinishLoadingWithResult:(NSString *)result
{
    //    NSLog(@"result----%@",result);
    NSError *error;
    NSData *data = [result dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&error];
    NSDictionary *sinaDic=jsonResponse;//[result objectFromJSONString];
    if ([request.tag isEqualToString:SINA_USERINFO]) {
        sinaUserInfo=[[NSMutableDictionary alloc] init];
        NSString *token=[LocalDataUtility getValueForKey:@"sinaAccessToken"];
        if ([sinaDic isExitKey:@"avatar_hd"]) {
            [sinaUserInfo setObject:[LocalDataUtility getValueForKey:@"sinaId"] forKey:@"user_id"];
            [sinaUserInfo setObject:[sinaDic objectForKey:@"avatar_hd"] forKey:@"avatar_url"];
            [sinaUserInfo setObject:[sinaDic objectForKey:@"screen_name"] forKey:@"nick_name"];
            [sinaUserInfo setObject:[[sinaDic objectForKey:@"gender"] isEqualToString:@"m"]?@"1":@"0" forKey:@"gender"];
            [sinaUserInfo setObject:[sinaDic objectForKey:@"description"] forKey:@"sign"];
            [sinaUserInfo setObject:token forKey:@"accessToken"];
            [LocalDataUtility setValue:[sinaDic objectForKey:@"screen_name"] forKey:@"sinaNickName"];
        }
        //    [self.delegate userInfo:sinaUserInfo];
        if (self.delegate==nil) {//刷新accesstoken
            [self refreshAccessToken:token type:Third_Part_Sina];
            return;
        }
        if ([self.delegate respondsToSelector:@selector(thirdpartId:type:nickName:acessToken:)]) {
            [self.delegate thirdpartId:[LocalDataUtility getValueForKey:@"sinaId"] type:Third_Part_Sina nickName:[LocalDataUtility getValueForKey:@"sinaNickName"] acessToken:[LocalDataUtility getValueForKey:@"sinaAccessToken"]];
        }
    }else if([request.tag isEqualToString:SINA_SHARE_CONTENT]){
//        DLog(@"发微博信息成功");
        if ([self.delegate respondsToSelector:@selector(shareSuccess:)]) {
            [self.delegate shareSuccess:Third_Part_Sina];
        }
    }else if([request.tag isEqualToString:SINA_PRIVATE_MESSAGE]){
        DLog(@"邀请成功");
    }else if([request.tag isEqualToString:SINA_EXPIRE]){
//        DLog(@"sinaDic-----%@",sinaDic);
        if (sinaDic&&[[sinaDic objectForKey:@"expire_in"] integerValue]<=0) {
            UIAlertView *expiredAlert=[[UIAlertView alloc] initWithTitle:@"微博授权信息已过期" message:@"是否重新授权？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"重新绑定", nil];
            expiredAlert.tag=100;
            [expiredAlert show];
        }
    }
}

-(void)bindUserInfo:(int)mtype value:(NSString *)value nickName:(NSString *)nickName isBind:(BOOL)isBind accessToken:(NSString *)accessToken
{
    if (mtype==Third_Part_Sina) {
        [LocalDataUtility setValue:value forKey:@"sinaId"];
        [LocalDataUtility setValue:nickName forKey:@"sinaNickName"];
        [LocalDataUtility setValue:accessToken forKey:@"sinaAccessToken"];
    }else if (mtype==Third_Part_QQ) {
        [LocalDataUtility setValue:value forKey:@"qqId"];
        [LocalDataUtility setValue:nickName forKey:@"qqNickName"];
        [LocalDataUtility setValue:accessToken forKey:@"qqAccessToken"];
    }
}

//2.00BrAquChnxEXD9b6ed84a850efS7L
-(void)refreshAccessToken:(NSString *)accessToken type:(NSInteger)type
{
    if (type==Third_Part_Sina) {
        [LocalDataUtility setValue:accessToken forKey:@"sinaAccessToken"];
    }else if (type==Third_Part_QQ) {
        [LocalDataUtility setValue:accessToken forKey:@"qqAccessToken"];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==100) {
        if (buttonIndex!=alertView.cancelButtonIndex) {
            self.delegate=nil;
            [self loginSina];
        }else{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"USER_LOGOUT" object:nil];
        }
    }
}

- (void)request:(WBHttpRequest *)request didFinishLoadingWithDataResult:(NSData *)data
{
    
}

-(void)shareContent:(NSString *)content imageUrl:(NSString *)imageUrl type:(int)type image:(UIImage *)image
{
//    DLog(@"%d-------length",content.length);
    if (imageUrl&&![imageUrl isEqualToString:@""]) {
        
        if (type == Third_Part_Sina) {
                        //新浪分享
//            DLog(@"%@-------sinaAccessToken",[XYCommon appDelegate].myInfo.sinaAccessToken);
            [WBHttpRequest requestWithAccessToken:[LocalDataUtility getValueForKey:@"sinaAccessToken"] url:@"https://api.weibo.com/2/statuses/upload_url_text.json" httpMethod:@"POST" params:[NSDictionary dictionaryWithObjectsAndKeys:content,@"status",imageUrl,@"url", nil] delegate:self withTag:SINA_SHARE_CONTENT];
        }else if(type == Third_Part_Tencent){
            
        }
    }else{
        //新浪分享
        if (type == Third_Part_Sina) {
            if (image) {
                [WBHttpRequest requestWithAccessToken:[LocalDataUtility getValueForKey:@"sinaAccessToken"] url:@"https://api.weibo.com/2/statuses/upload.json" httpMethod:@"POST" params:[NSDictionary dictionaryWithObjectsAndKeys:content,@"status",image,@"pic", nil] delegate:self withTag:SINA_SHARE_CONTENT];
            }else{
                [WBHttpRequest requestWithAccessToken:[LocalDataUtility getValueForKey:@"sinaAccessToken"] url:@"https://api.weibo.com/2/statuses/update.json" httpMethod:@"POST" params:[NSDictionary dictionaryWithObjectsAndKeys:content,@"status", nil] delegate:self withTag:SINA_SHARE_CONTENT];
            }
        }else if(type == Third_Part_Tencent){
            if (image){
                
            }else{
                
            }
        }
    }
}

#pragma mark -- QQ相关
- (void)loginQQ
{
    NSArray* permissions = [NSArray arrayWithObjects:
                     kOPEN_PERMISSION_GET_USER_INFO,
                     kOPEN_PERMISSION_GET_SIMPLE_USER_INFO,
                     kOPEN_PERMISSION_ADD_IDOL,
                     kOPEN_PERMISSION_ADD_PIC_T,
                     kOPEN_PERMISSION_ADD_SHARE,
                     kOPEN_PERMISSION_ADD_TOPIC,
                     kOPEN_PERMISSION_CHECK_PAGE_FANS,
                     kOPEN_PERMISSION_DEL_IDOL,
                     kOPEN_PERMISSION_DEL_T,
                     kOPEN_PERMISSION_GET_FANSLIST,
                     kOPEN_PERMISSION_GET_IDOLLIST,
                     kOPEN_PERMISSION_GET_INFO,
                     kOPEN_PERMISSION_GET_OTHER_INFO,
                     kOPEN_PERMISSION_GET_REPOST_LIST,
                     kOPEN_PERMISSION_GET_VIP_INFO,
                     kOPEN_PERMISSION_GET_VIP_RICH_INFO,
                     kOPEN_PERMISSION_GET_INTIMATE_FRIENDS_WEIBO,
                     kOPEN_PERMISSION_MATCH_NICK_TIPS_WEIBO,
                     nil];
    BOOL isSafari=NO;
    if ([TencentOAuth iphoneQQInstalled]) {
        if ([TencentOAuth iphoneQQSupportSSOLogin]) {
            isSafari=NO;
        }else{
            isSafari=YES;
        }
    }else{
        isSafari=YES;
    }
    [qqOAuth authorize:permissions inSafari:isSafari];
}

- (void)tencentDidLogin
{
    if (qqOAuth.accessToken && 0 != [qqOAuth.accessToken length]){
        [qqOAuth getUserInfo];
    }else{
        DLog(@"登录不成功 没有获取accesstoken");
        [self sendQQLoadRet:nil];
    }
}

- (void)tencentDidNotLogin:(BOOL)cancelled
{
	if (cancelled){
        DLog(@"用户取消登录");
        [self sendQQLoadRet:nil];
	}else {
		DLog(@"登录失败");
        [self sendQQLoadRet:nil];
	}
}

-(void)tencentDidNotNetWork
{
    DLog(@"无网络连接，请设置网络");
    [self sendQQLoadRet:nil];
}

-(void)tencentDidLogout
{
	DLog(@"退出登录成功，请重新登录");
}

- (void)getUserInfoResponse:(APIResponse*) response {
	if (response.retCode == URLREQUEST_SUCCEED){
		DLog(@"%@------getUserInfoResponse",response.jsonResponse);
        [self sendQQLoadRet:response.jsonResponse];
	}else{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"操作失败" message:[NSString stringWithFormat:@"%@", response.errorMsg] delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
		[alert show];
	}
    DLog(@"获取个人信息完成");
}

-(void)sendQQLoadRet:(NSDictionary *)userDic
{
    if ([self.delegate respondsToSelector:@selector(thirdpartId:type:nickName:acessToken:)]) {
        qqUserInfo=[[NSMutableDictionary alloc] init];
        if (userDic) {
            [qqUserInfo setObject:[userDic objectForKey:@"figureurl_2"] forKey:@"avatar_url"];
            [qqUserInfo setObject:qqOAuth.openId forKey:@"user_id"];
            [qqUserInfo setObject:[userDic objectForKey:@"nickname"] forKey:@"nick_name"];//C6DB4ECAF6C20E980DC7007C50DB9B85
            [qqUserInfo setObject:@"" forKey:@"sign"];
            [qqUserInfo setObject:[[userDic objectForKey:@"gender"] isEqualToString:@"男"]?@"1":@"0" forKey:@"gender"];
            [qqUserInfo setObject:qqOAuth.accessToken forKey:@"accessToken"];
            [self.delegate thirdpartId:qqOAuth.openId type:Third_Part_QQ nickName:[userDic objectForKey:@"nickname"] acessToken:qqOAuth.accessToken];
            [LocalDataUtility setValue:[userDic objectForKey:@"nickname"] forKey:@"qqNickName"];
        }else{
            [self.delegate thirdpartId:nil type:Third_Part_QQ nickName:nil acessToken:nil];
        }
    }
}


@end
