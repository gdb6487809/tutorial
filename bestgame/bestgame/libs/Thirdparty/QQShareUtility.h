//
//  QQShareUtility.h
//  FilmWithYou
//
//  Created by DongboGuan on 14-9-4.
//  Copyright (c) 2014年 DongboGuan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TencentOpenAPI/QQApi.h>
#import <TencentOpenAPI/QQApiInterfaceObject.h>
#import <TencentOpenAPI/QQApiInterface.h>

@interface QQShareUtility : NSObject

+(void)shareWechatImage:(UIImage *)image desc:(NSString *)desc title:(NSString *)title;
+(void)shareWechatUrl:(NSString *)url imageUrl:(NSString *)imageUrl desc:(NSString *)desc title:(NSString *)title;

@end
