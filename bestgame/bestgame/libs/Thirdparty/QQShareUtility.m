//
//  QQShareUtility.m
//  FilmWithYou
//
//  Created by DongboGuan on 14-9-4.
//  Copyright (c) 2014年 DongboGuan. All rights reserved.
//

#import "QQShareUtility.h"

@implementation QQShareUtility

+(void)shareWechatUrl:(NSString *)url imageUrl:(NSString *)imageUrl desc:(NSString *)desc title:(NSString *)title
{
    //分享图预览图URL地址
    QQApiNewsObject *newsObj = [QQApiNewsObject
                                objectWithURL:[NSURL URLWithString:url]
                                title: title
                                description:desc
                                previewImageURL:[NSURL URLWithString:imageUrl]];
    SendMessageToQQReq *req = [SendMessageToQQReq reqWithContent:newsObj];
    //将内容分享到qq
    //QQApiSendResultCode sent = [QQApiInterface sendReq:req];
    //将内容分享到qzone
    QQApiSendResultCode sent = [QQApiInterface sendReq:req];
    if (sent==EQQAPISENDSUCESS) {
        [[SingletonUtility instance].appDelegate.window showTips:@"分享到说说成功"];
    }
}

+(void)shareWechatImage:(UIImage *)image desc:(NSString *)desc title:(NSString *)title
{
    NSData *imgData = UIImageJPEGRepresentation(image,0.3) ;
    //
    QQApiImageObject *imgObj = [QQApiImageObject objectWithData:UIImageJPEGRepresentation(image,1)
                                               previewImageData:imgData
                                                          title:title
                                                    description:desc];
    SendMessageToQQReq *req = [SendMessageToQQReq reqWithContent:imgObj];
    //将内容分享到qq
    QQApiSendResultCode sent = [QQApiInterface sendReq:req];
    if (sent==EQQAPISENDSUCESS) {
        [[SingletonUtility instance].appDelegate.window showTips:@"分享到说说成功"];
    }
}

@end
