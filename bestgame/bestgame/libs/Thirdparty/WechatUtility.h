//
//  WechatUtility.h
//  WPWProject
//
//  Created by 关 东波 on 14-1-3.
//  Copyright (c) 2014年 Mr.Lu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WXApi.h"

@interface WechatUtility : NSObject<UIAlertViewDelegate,WXApiDelegate>

+(void)shareWechatUrl:(NSString *)url image:(UIImage *)image desc:(NSString *)desc title:(NSString *)title scene:(int)scene;
+(void)shareWechatImage:(UIImage *)image desc:(NSString *)desc title:(NSString *)title scene:(int)scene;
+(void)wechatSendImage:(UIImage *)image scene:(int)scene;
+(void)sendTextContent:(NSString*)desc;
+(void)shareApp;
-(BOOL)handleOpenURL:(NSURL *)url wxApiDelegate:(id<WXApiDelegate>)wxApiDelegate;

@end

@interface WechatHandler : NSObject<UIAlertViewDelegate>

+(WechatHandler *)instance;
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

@end
