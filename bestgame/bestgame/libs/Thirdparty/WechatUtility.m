//
//  WechatUtility.m
//  WPWProject
//
//  Created by 关 东波 on 14-1-3.
//  Copyright (c) 2014年 Mr.Lu. All rights reserved.
//

#import "WechatUtility.h"
#import "WXApi.h"
#import "UIImage+Compress.h"
#import "ShareSheetView.h"
@implementation WechatUtility

+(void)shareWechatUrl:(NSString *)url image:(UIImage *)image desc:(NSString *)desc title:(NSString *)title scene:(int)scene
{
    if ([WechatUtility hasStalledWechat]==NO) {
        return;
    }
    WXMediaMessage *message = [WXMediaMessage message];
    NSData *data = UIImageJPEGRepresentation(image,0.8);
    [message setThumbImage:[[UIImage imageWithData:data] compressedImage]];
    message.title=title?title:@"";
    message.description =desc ;
    WXWebpageObject *ext = [WXWebpageObject object];
    if (url) {
        ext.webpageUrl=url;
    }else{
        ext.webpageUrl=@"http://3ovie.com/prdoorhost.php/pbp/download?wx.qq.com";
    }
    message.mediaObject = ext;
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = scene;
    [WXApi sendReq:req];
}

+(void)shareWechatImage:(UIImage *)image desc:(NSString *)desc title:(NSString *)title scene:(int)scene
{
    if ([WechatUtility hasStalledWechat]==NO) {
        return;
    }
    WXMediaMessage *message = [WXMediaMessage message];
    //        NSData *data = UIImageJPEGRepresentation(image,0.8);
    //        [message setThumbImage:[[UIImage imageWithData:data] compressedImage]];
    message.title=desc;
    message.description = title;
    WXImageObject *ext = [WXImageObject object];
    ext.imageData=UIImageJPEGRepresentation(image,0.8);
    message.mediaObject = ext;
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = scene;
    [WXApi sendReq:req];
}

+(void)wechatSendImage:(UIImage *)image scene:(int)scene
{
    if ([WechatUtility hasStalledWechat]==NO) {
        return;
    }
    WXMediaMessage *message = [WXMediaMessage message];
    [message setThumbImage:[[UIImage imageWithData:UIImageJPEGRepresentation(image, 0.6)] compressedImage]];
    
    WXImageObject *ext = [WXImageObject object];
    ext.imageData =UIImageJPEGRepresentation(image,0.6);
    message.mediaObject = ext;
    
    SendMessageToWXReq* req =[[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = scene;
    
    [WXApi sendReq:req];
}

+(BOOL)hasStalledWechat
{
    //未安装微信提醒用户下载微信
    if (![WXApi isWXAppInstalled]){
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"您的设备未安装微信" delegate:[WechatHandler instance] cancelButtonTitle:@"以后再说" otherButtonTitles:@"前往下载微信", nil];
        [alert setTag:123];
        [alert show];
        return NO;
    }
    if (![WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled])
    {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"您的微信版本不支持分享功能" delegate:[WechatHandler instance] cancelButtonTitle:@"以后再说" otherButtonTitles:@"下载最新版微信", nil];
        [alert setTag:124];
        [alert show];
        return NO;
    }
    return YES;
}

+(void)shareApp
{
    if ([self hasStalledWechat]==NO) {
        return;
    }
    WXMediaMessage *message = [WXMediaMessage message];
    NSData *data = UIImageJPEGRepresentation([UIImage imageNamed:@"icon"],0.8);
    [message setThumbImage:[[UIImage imageWithData:data] compressedImage]];
    message.title=@"陪你看 - 跟好友开房看片";
    message.description = @"电影-艺术，陪伴-温暖，与“TA”一起品味艺术，拥抱温暖，只在陪你看";
    WXWebpageObject *ext = [WXWebpageObject object];
    ext.webpageUrl=@"https://itunes.apple.com/us/app/pei-ni-kan-dian-ying-bu-zai/id927292401?l=zh&ls=1&mt=8";
    message.mediaObject = ext;
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = WXSceneSession;
    [WXApi sendReq:req];
}



//-(void)AddWechat:(NSNotification *)sender
//{
//    receiveString = [[sender userInfo] objectForKey:@"PYQ"]; 
//}
-(void) onResp:(BaseResp*)resp
{
    if([resp isKindOfClass:[SendMessageToWXResp class]])
    {
        if (resp.errCode==WXSuccess) {
            
        }
        [[NSNotificationCenter defaultCenter]postNotificationName:@"SEND_WECHAT_SUCCESS" object:nil];
    }
}

+(void)sendTextContent:(NSString*)desc
{
    
}

-(BOOL)handleOpenURL:(NSURL *)url wxApiDelegate:(id<WXApiDelegate>)wxApiDelegate
{
    [WXApi handleOpenURL:url delegate:self];
    return YES;
}

@end

@implementation WechatHandler

static WechatHandler *wechatHandler;

+(WechatHandler *)instance
{
    @synchronized (self)
    {
        if (wechatHandler == nil)
        {
            wechatHandler = [[self alloc] init];
        }
    }
    
    return wechatHandler;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != alertView.cancelButtonIndex) {
        if (IOS7==NO) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@",kAppleID]]];
        }else{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/app/id%@?at=10l6dK",kAppleID]]];;
        }
    }
}

@end
