//
//  ThirdPartLogin.h
//  navi
//
//  Created by 关 东波 on 13-11-14.
//  Copyright (c) 2013年 关 东波. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "WeiboSDK.h"
#import <TencentOpenAPI/QQApi.h>
#import <TencentOpenAPI/QQApiInterfaceObject.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "NetworkManager.h"

#define SINA_AUTH @"sina_auth"
#define SINA_USERINFO @"sina_userinfo"
#define SINA_EXPIRE @"sina_expire"
#define SINA_FRIEND_LIST @"sina_friend_list"
#define SINA_SHARE_CONTENT @"sina_share_content"
#define SINA_PRIVATE_MESSAGE @"sina_private_message"

#define QQ_AUTH @"qq_auth"
#define QQ_USERINFO @"qq_userinfo"
#define QQ_EXPIRE @"qq_expire"
#define QQ_FRIEND_LIST @"qq_friend_list"
#define QQ_SHARE_CONTENT @"qq_share_content"
#define QQ_PRIVATE_MESSAGE @"qq_private_message"

typedef enum
{
    Third_Part_Sina = 1,
    Third_Part_Tencent = 2,
    Third_Part_Phone = 3,
    Third_Part_QQ = 4,
}Third_Part_Type;

@protocol ThirdPartLoginDelegate;

@interface ThirdPartLogin : NSObject<WeiboSDKDelegate,WBHttpRequestDelegate,TencentSessionDelegate,TencentLoginDelegate>
{
    NSMutableDictionary *sinaUserInfo;
    NSMutableDictionary *qqUserInfo;
    UIImage *shareImage;
    NSString *sinaRequestUrl;
    NSString *tencentRequestUrl;
    TencentOAuth *qqOAuth;
}

@property(nonatomic,assign)id<ThirdPartLoginDelegate> delegate;
@property(nonatomic,strong) NSMutableDictionary *sinaUserInfo;
@property(nonatomic,strong) NSMutableDictionary *qqUserInfo;
@property(nonatomic,strong) TencentOAuth *qqOAuth;

+ (ThirdPartLogin *)instance;

- (void)loginSina;
- (void)validateSinaExpire;
- (void)loginQQ;
- (void)getSinaUserInfo;

- (void)shareContent:(NSString *)content imageUrl:(NSString *)imageUrl type:(int)type image:(UIImage *)image;


@end

@protocol ThirdPartLoginDelegate <NSObject>

@optional
-(void)thirdpartId:(NSString *)thirdpartId type:(Third_Part_Type)type nickName:(NSString *)nickName acessToken:(NSString *)accessToken;

//头像：avatar_url    昵称：nick_name    生日：birthday     性别：gender      签名：sign
-(void)userInfo:(NSDictionary *)userInfo;

-(void)getThirdPartFriend:(Third_Part_Type)type friendList:(NSMutableArray *)friendList uthirdpart_id:(NSString *)uthirdpart_id;

-(void)shareSuccess:(Third_Part_Type)type;
@end
