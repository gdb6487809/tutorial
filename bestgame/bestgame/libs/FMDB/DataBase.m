//
//  DataBase.m
//  SQList Data Controller
//
//  Created by ibokan on 13-2-25.
//  Copyright (c) 2013年 ibokan. All rights reserved.
//

#import "DataBase.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"

static FMDatabase * db = nil;
static FMDatabaseQueue *dbQuene=nil;

@implementation DataBase

+(NSString *)databaseFilePath
{
    NSArray *filePath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [filePath objectAtIndex:0];
    NSString *dbFilePath = [documentPath stringByAppendingPathComponent:DB_NAME];
//    DLog(@"%@-----dbFilePath",dbFilePath);
    if ([[NSFileManager defaultManager] fileExistsAtPath:dbFilePath] == NO) {
        NSString *sourcePath = [[NSBundle mainBundle] pathForResource:DB_NAME ofType:@""];
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:dbFilePath error:nil];
    }
    return dbFilePath;
}

+(void)creatDatabase
{
    db = [FMDatabase databaseWithPath:[self databaseFilePath]];
}

+(FMDatabase *)getDataBase
{
    //先判断数据库是否存在，如果不存在，创建数据库
    if (!db)
    {
        [self creatDatabase];
        //为数据库设置缓存，提高查询效率
        [db setShouldCacheStatements:YES];
    }
    
    if (![db open])
    {
        DLog(@"数据库打开失败");
        return nil;
    }
    
    return db;
}

+(void)createDatabaseQuene
{
//    [self getDataBase];
    if (!dbQuene) {
        dbQuene = [FMDatabaseQueue databaseQueueWithPath:[self databaseFilePath]];
        [DataBase updateTables];
    }
}

+(FMDatabaseQueue *)getDataBaseQuene
{
    if (!dbQuene)
    {
        [self createDatabaseQuene];
    }
    
    return dbQuene;
}

+(void)updateTables  //创建数据库里的表
{
//    NSTimeInterval start=[[NSDate new] timeIntervalSince1970];
//    DLog(@"%f-----startTimeStamp",start);
    NSString *script = [[NSBundle mainBundle] pathForResource:@"updateScript_1.0" ofType:@"plist"];//更改数据库结构的脚本文件
    NSDictionary *updateTables=[[NSDictionary alloc]initWithContentsOfFile:script];
    
    NSArray *keys;
    id key, value;
    
    keys = [updateTables allKeys];
    for (int i = 0; i < [keys count]; i++)//遍历脚本的内容
    {
        key = [keys objectAtIndex: i];
        value = [updateTables objectForKey: key];
        [self oprTable:value];//增加字段到某个表中
    }
//    NSTimeInterval end=[[NSDate new] timeIntervalSince1970];
//    DLog(@"%f-----endTimeStamp",end);
//    DLog(@"%f-----diff",end-start);
}

+ (BOOL)isTableExist:(NSString *)tableName
{
    NSString *sql = [NSString stringWithFormat:@"select count(*) as 'count' from sqlite_master where type ='table' and name = '%@'",tableName];
    FMResultSet *rs = [db executeQuery:sql];
    while ([rs next])
    {
        // just print out what we've got in a number of formats.
        NSInteger count = [rs intForColumn:@"count"];
        NSLog(@"isTableOK %@", @(count));
        
        if (0 == count)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    
    return NO;
}

+ (BOOL)isColumnExist:(NSString *)column table:(NSString *)table
{
    NSString *sql = [NSString stringWithFormat:@"select times from %@",table];
    FMResultSet *rs = [db executeQuery:sql];
    for (int i=0;i<rs.columnCount;i++){
        if ([[rs columnNameForIndex:i] isEqualToString:column]){
            return YES;
            break;
        }
    }
    return NO;
}

+ (void)oprTable:(NSString *)oprSQL
{
    [dbQuene inTransaction:^(FMDatabase *db, BOOL *rollback) {
        if ([db open]) {
            [db executeUpdate:oprSQL];
            if([db hadError])
            {
//                NSLog(@"Error %d : %@",[db lastErrorCode],[db lastErrorMessage]);
            }
            [db commit];
        }
    }];
}



@end
