//
//  AnalysisModel.m
//  analysis
//
//  Created by DongboGuan on 15/2/5.
//  Copyright (c) 2015年 DongboGuan. All rights reserved.
//

#import "AnalysisModel.h"
#import "NSDictionary+IsExitKey.h"

@implementation AnalysisModel

-(AnalysisModel *)initWithDic:(NSDictionary *)dic
{
    self = [super init];
    if (self) {
        self.dataiId=[dic isExitKey:@"id"]?[[dic valueForKey:@"id"] integerValue]:-1;
        self.inviteallcount=[dic isExitKey:@"inviteallcount"]?[[dic valueForKey:@"inviteallcount"] integerValue]:-1;
        self.interesttopiccount=[dic isExitKey:@"interesttopiccount"]?[[dic valueForKey:@"interesttopiccount"] integerValue]:-1;
        self.activecount=[dic isExitKey:@"activecount"]?[[dic valueForKey:@"activecount"] integerValue]:-1;
        self.ordercount=[dic isExitKey:@"ordercount"]?[[dic valueForKey:@"ordercount"] integerValue]:-1;
        self.interesttopiclaudcount=[dic isExitKey:@"interesttopiclaudcount"]?[[dic valueForKey:@"interesttopiclaudcount"] integerValue]:-1;
        self.interesttopicreplycount=[dic isExitKey:@"interesttopicreplycount"]?[[dic valueForKey:@"interesttopicreplycount"] integerValue]:-1;
        self.filmcriticcount=[dic isExitKey:@"filmcriticcount"]?[[dic valueForKey:@"filmcriticcount"] integerValue]:-1;
        self.registcount=[dic isExitKey:@"registcount"]?[[dic valueForKey:@"registcount"] integerValue]:-1;
        self.invitecount=[dic isExitKey:@"invitecount"]?[[dic valueForKey:@"invitecount"] integerValue]:-1;
        self.date = [dic isExitKey:@"date"]?[dic valueForKey:@"date"]:@"今天";
        self.opened=NO;
    }
    return self;
}


@end

