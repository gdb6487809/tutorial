//
//  AnalysisModel.h
//  analysis
//
//  Created by DongboGuan on 15/2/5.
//  Copyright (c) 2015年 DongboGuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnalysisModel : NSObject

@property(nonatomic,assign)NSInteger dataiId;//
@property(nonatomic,assign)NSInteger inviteallcount;//
@property(nonatomic,assign)NSInteger interesttopiccount;//
@property(nonatomic,assign)NSInteger activecount;//
@property(nonatomic,assign)NSInteger ordercount;//
@property(nonatomic,assign)NSInteger interesttopiclaudcount;//
@property(nonatomic,assign)NSInteger interesttopicreplycount;//
@property(nonatomic,assign)NSInteger filmcriticcount;//
@property(nonatomic,assign)NSInteger registcount;//
@property(nonatomic,assign)NSInteger invitecount;//
@property(nonatomic,strong)NSString *date;//

@property(nonatomic,assign)BOOL opened;//

-(AnalysisModel *)initWithDic:(NSDictionary *)dic;

@end