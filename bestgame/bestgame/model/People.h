//
//  People.h
//  WPWProject
//
//  Created by Mr.Lu on 13-7-1.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSUInteger, USER_TYPE) {
    USER_TYPE_COMMON = 0,
    USER_TYPE_MANAGER = 1
};

@interface People : NSObject

@property (copy, nonatomic)NSString * headpic;//用户头像
@property (copy, nonatomic)NSString * lastcoord; //用户定位点  （1，1）
@property (copy, nonatomic)NSString * longitud;  //经度
@property (copy, nonatomic)NSString * latitude; //纬度
@property (assign, nonatomic)int lastLoginTime; //用户定位点  （1，1）
@property (assign, nonatomic)int userid;//用户id
@property (assign, nonatomic)USER_TYPE userType;//用户id
@property (copy, nonatomic)NSString * gender; //性别
@property (copy, nonatomic)NSString * userName; //用户名字
@property (copy, nonatomic)NSString * des;//描述
@property (copy, nonatomic)NSString * phone;//手机
@property (assign, nonatomic)int isFriend;
@property (assign, nonatomic)int age;//年龄

@property (copy, nonatomic)NSString *sinaId;//新浪id
@property (copy, nonatomic)NSString *sinaAccessToken;//新浪昵称
@property (copy, nonatomic)NSString *sinaName;//新浪昵称
@property (copy, nonatomic)NSString *qqId;//QQid
@property (copy, nonatomic)NSString *qqAccessToken;//QQ昵称
@property (copy, nonatomic)NSString *qqName;//QQ昵称
@property (copy, nonatomic)NSString *email;//邮箱
@property (copy, nonatomic)NSString *backgroundPic;//邮箱
@property (copy, nonatomic)NSString *headpicThumb;//邮箱
@property (assign, nonatomic)int userNum;//用户id
@property (copy, nonatomic)NSString *remark;//用户备注
@property (copy, nonatomic)NSString *birthday;//生日



//初始化对象
-(People *)initWithDic:(NSDictionary *)dic; 


@end
