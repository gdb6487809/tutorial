//
//  People.m
//  WPWProject
//
//  Created by Mr.Lu on 13-7-1.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import "People.h"
#import "NSDictionary+IsExitKey.h"
#import "DataBase.h"
#import "FMDatabaseAdditions.h"
#import "NSString+Utility.h"

@implementation People

//网络初始话对象

-(People *)initWithDic:(NSDictionary *)dic
{
    self = [super init];
    if (self) {
        self.headpic = [dic isExitKey:@"HEADPIC"]?[dic valueForKey:@"HEADPIC"]:@"";
        self.lastcoord = [dic isExitKey:@"LASTCOORD"]?[dic valueForKey:@"LASTCOORD"]:@"";
        self.longitud = [dic isExitKey:@"LONGITUD"]?[dic valueForKey:@"LONGITUD"]:@"";
        self.latitude = [dic isExitKey:@"LATITUDE"]?[dic valueForKey:@"LATITUDE"]:@"";
        ////在字典里如果有这个字段，就把他添加否则为空
        self.lastLoginTime = [dic isExitKey:@"LASTLOGINTIME"]?(int)([[dic valueForKey:@"LASTLOGINTIME"] longLongValue]/1000):-1;
        
        self.userName = [dic isExitKey:@"USERNAME"]?[dic valueForKey:@"USERNAME"]:@"";
        self.userid = [dic isExitKey:@"USERID"]?[[dic valueForKey:@"USERID"]intValue]:[[dic valueForKey:@"ID"]intValue];
        if (self.userid==0) {
            self.userid = [dic isExitKey:@"UID"]?[[dic valueForKey:@"UID"]intValue]:0;
        }
        self.userType = [dic isExitKey:@"USERTYPE"]?[[dic valueForKey:@"USERTYPE"]intValue]:0;
       
        self.gender = [dic isExitKey:@"GENDER"]?[dic valueForKey:@"GENDER"]:@"";
        self.des = [dic isExitKey:@"DES"]?[dic valueForKey:@"DES"]:@"";
        self.age = [dic isExitKey:@"AGE"]?[[dic valueForKey:@"AGE"]intValue]:0;
        self.phone=[dic isExitKey:@"PHONE"]?[dic valueForKey:@"PHONE"]:@"";
        self.isFriend=[dic isExitKey:@"ISFRIEND"]?[[dic valueForKey:@"ISFRIEND"] intValue]:-1;
        self.birthday = [dic isExitKey:@"BIRTHDAY"]?([EmptyUtility isEmpty:[dic valueForKey:@"BIRTHDAY"]]?@"":[dic valueForKey:@"BIRTHDAY"]):@"";
        
        self.sinaId = [dic isExitKey:@"SINAID"]?([EmptyUtility isEmpty:[dic valueForKey:@"SINAID"]]?@"":[dic valueForKey:@"SINAID"]):@"";
        self.sinaName = [dic isExitKey:@"SINANICKNAME"]?([EmptyUtility isEmpty:[dic valueForKey:@"SINANICKNAME"]]?@"":[dic valueForKey:@"SINANICKNAME"]):@"";
        self.sinaAccessToken = [dic isExitKey:@"SINAACCESSTOKEN"]?([EmptyUtility isEmpty:[dic valueForKey:@"SINAACCESSTOKEN"]]?@"":[dic valueForKey:@"SINAACCESSTOKEN"]):@"";
        self.qqId = [dic isExitKey:@"TC_QQID"]?([EmptyUtility isEmpty:[dic valueForKey:@"TC_QQID"]]?@"":[dic valueForKey:@"TC_QQID"]):@"";
        self.qqName = [dic isExitKey:@"TC_QQNICKNAME"]?([EmptyUtility isEmpty:[dic valueForKey:@"TC_QQNICKNAME"]]?@"":[dic valueForKey:@"TC_QQNICKNAME"]):@"";
        self.qqAccessToken = [dic isExitKey:@"TC_QQACCESSTOKEN"]?([EmptyUtility isEmpty:[dic valueForKey:@"TC_QQACCESSTOKEN"]]?@"":[dic valueForKey:@"TC_QQACCESSTOKEN"]):@"";
        self.email = [dic isExitKey:@"EMAIL"]?([EmptyUtility isEmpty:[dic valueForKey:@"EMAIL"]]?@"":[dic valueForKey:@"EMAIL"]):@"";
        self.backgroundPic = [dic isExitKey:@"BGPIC"]?([EmptyUtility isEmpty:[dic valueForKey:@"BGPIC"]]?@"":[dic valueForKey:@"BGPIC"]):@"";
        self.headpicThumb = [dic isExitKey:@"HEADPICTHUMB"]?([EmptyUtility isEmpty:[dic valueForKey:@"HEADPICTHUMB"]]?self.headpic:[dic valueForKey:@"HEADPICTHUMB"]):self.headpic;
        self.userNum = [dic isExitKey:@"USERNUM"]?[[dic valueForKey:@"USERNUM"] intValue]:0;
        self.remark = [dic isExitKey:@"REMARKNAME"]?([EmptyUtility isEmpty:[dic valueForKey:@"REMARKNAME"]]?self.userName:[dic valueForKey:@"REMARKNAME"]):self.userName;
    }
    return self;
}

+(void)insertPeopleShortInfo:(People *)people
{
    FMDatabaseQueue * dbQueue = [DataBase getDataBaseQuene];
    __block int ret=0;
    [dbQueue inDatabase:^(FMDatabase *db)   {
        if ([db open]) {
            ret = [db intForQuery:@"select count(*) from PeopleCacheList Where UserID = ? and MyID = ?",[NSNumber numberWithInt:people.userid],1];
        }
    }];
    
    [dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        if ([db open]) {
            if (ret) {
                NSMutableString *sqlStr=[[NSMutableString alloc] initWithString:@"UPDATE PeopleCacheList SET "];
                if (![EmptyUtility isEmpty:people.headpic]) {
                    [sqlStr appendFormat:@"headpic = '%@' ,",people.headpic];
                }
                if (![EmptyUtility isEmpty:people.lastcoord]) {
                    [sqlStr appendFormat:@"LastCoord = '%@' ,",people.lastcoord];
                }
                if (people.lastLoginTime>0) {
                    [sqlStr appendFormat:@"LastLoginTime = '%d' ,",people.lastLoginTime];
                }
                
                if (![EmptyUtility isEmpty:people.gender]) {
                    [sqlStr appendFormat:@"Sex = '%@' ,",people.gender];
                }
                if (![EmptyUtility isEmpty:people.userName]) {
                    [sqlStr appendFormat:@"UserName = '%@' ,",[people.userName removeQuata]];
                }
                if (![EmptyUtility isEmpty:people.des]) {
                    [sqlStr appendFormat:@"Desc = '%@' ,",[people.des removeQuata]];
                }
                if (![EmptyUtility isEmpty:people.phone]) {
                    [sqlStr appendFormat:@"Phone = '%@' ,",people.phone];
                }
                if (![EmptyUtility isEmpty:people.backgroundPic]) {
                    [sqlStr appendFormat:@"BackgroundPic = '%@' ,",people.backgroundPic];
                }
                if (![EmptyUtility isEmpty:people.headpicThumb]) {
                    [sqlStr appendFormat:@"HeadpicThumb = '%@' ,",people.headpicThumb];
                }
                if (people.userNum!=0) {
                    [sqlStr appendFormat:@"UserNum = '%d' ,",people.userNum];
                }
                if (![EmptyUtility isEmpty:people.remark]) {
                    [sqlStr appendFormat:@"RemarkName = '%@' ,",[people.remark removeQuata]];
                }
                if (![EmptyUtility isEmpty:people.birthday]) {
                    [sqlStr appendFormat:@"Birthday = '%@' ,",people.birthday];
                }
                if (![EmptyUtility isEmpty:people.email]) {
                    [sqlStr appendFormat:@"Email = '%@' ,",people.email];
                }
                if (![EmptyUtility isEmpty:people.sinaId]) {
                    [sqlStr appendFormat:@"SinaId = '%@' ,",people.sinaId];
                }
                if (![EmptyUtility isEmpty:people.sinaAccessToken]) {
                    [sqlStr appendFormat:@"SinaAccessToken = '%@' ,",people.sinaAccessToken];
                }
                if (![EmptyUtility isEmpty:people.sinaName]) {
                    [sqlStr appendFormat:@"SinaNickName = '%@' ,",people.sinaName];
                }
                if (![EmptyUtility isEmpty:people.qqId]) {
                    [sqlStr appendFormat:@"QQId = '%@' ,",people.qqId];
                }
                if (![EmptyUtility isEmpty:people.qqAccessToken]) {
                    [sqlStr appendFormat:@"QQAccessToken = '%@' ,",people.qqAccessToken];
                }
                if (![EmptyUtility isEmpty:people.qqName]) {
                    [sqlStr appendFormat:@"QQNickName = '%@' ,",people.qqName];
                }
                if (people.isFriend != -1) {
                    [sqlStr appendFormat:@"IsFriend = '%d' ,",people.isFriend];
                }
                [sqlStr appendFormat:@"userType = '%@' ,",@(people.userType)];
                
                [sqlStr appendFormat:@"UserID = '%d' ",people.userid];
                
                [sqlStr appendFormat:@" WHERE UserID = '%d' and MyID = '%d' ",people.userid,1];
                [db executeUpdate:sqlStr];
                //                DLog(@"%@------INSERT PeopleCacheList",db.lastErrorMessage);
            }
            else
            {
                [db executeUpdate:@"INSERT INTO PeopleCacheList ( headpic ,LastCoord ,LastLoginTime,UserID,Gender,UserName,MyID,Age,Desc,Phone,IsFriend,BackgroundPic,HeadpicThumb,UserNum,RemarkName,Birthday,Email,SinaId,SinaAccessToken,SinaNickName,QQId,QQAccessToken,QQNickName,userType) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,)",people.headpic,people.lastcoord,[NSNumber numberWithInt:people.lastLoginTime],[NSNumber numberWithInt:people.userid],people.gender,people.userName,[NSNumber numberWithInt:1],[NSNumber numberWithInt:people.age],people.des,people.phone,[NSNumber numberWithInt:people.isFriend],people.backgroundPic,people.headpicThumb,[NSNumber numberWithInt:people.userNum],people.remark,people.birthday,people.email,people.sinaId,people.sinaAccessToken,people.sinaName,people.qqId,people.qqAccessToken,people.qqName,[NSNumber numberWithInt:people.userType]];
            }
            //            DLog(@"%@------INSERT PeopleCacheList",db.lastErrorMessage);
        }
    }];
    
}


@end
