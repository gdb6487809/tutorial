//
//  CommonUtility.h
//  WPWProject
//
//  Created by 豪 on 13-11-21.
//  Copyright (c) 2013年 Mr.Lu. All rights reserved.
//

#import "NetworkManager.h"
#import "EmptyUtility.h"
#import "PullingRefreshTableView.h"
#import "UIView+FrameHelper.h"
#import "SingletonUtility.h"
#import "LocalDataUtility.h"
#import "UIView+Utility.h"

#ifdef DEBUG
#define DLog( s, ...) NSLog(@"<%@ :%d> %@",[[NSString stringWithUTF8String:__FILE__]lastPathComponent],__LINE__,[NSString stringWithFormat:(s),##__VA_ARGS__]);
#else
#define DLog(s,...)
#endif

#define RGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]
#define IMAGENAMED(name) [UIImage imageNamed:name]

#define DEFAULT_BLUE_COLOR RGBACOLOR(0,138,255,1)
#define DEFAULT_LIGHT_BLUE_COLOR RGBACOLOR(0,138,255,0.2)
#define DEFAULT_BORDER_COLOR RGBACOLOR(236,236,236,1)

#define kDeviceWidth [UIScreen mainScreen].bounds.size.width      //界面宽度
#define kDeviceHeight ([UIScreen mainScreen].bounds.size.height-20) //界面高度
#define kDeviceScaleFactor (MIN(kDeviceWidth, kDeviceHeight+20)/320)

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size)||CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size)||CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size)) : NO)

#define iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size)||CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size)) : NO)

#define iPhone6Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242,2208), [[UIScreen mainScreen] currentMode].size) : NO)

#define IOS_VERSION [[UIDevice currentDevice].systemVersion floatValue]
#define IOS8 (IOS_VERSION>=8.0)
#define IOS7 (IOS_VERSION>=7.0)

#define kAppleID @""
#define QQ_APP_ID @""
#define SINA_APP_ID @""

#define DB_NAME @"db_1.0.sqlite"

static NSString *kHomeHost=@"http://www.3ovie.com/mt/";      //线上正式

