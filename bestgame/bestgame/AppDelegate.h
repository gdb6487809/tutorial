//
//  AppDelegate.h
//  bestgame
//
//  Created by DongboGuan on 15/6/27.
//  Copyright (c) 2015年 DongboGuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

