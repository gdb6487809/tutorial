//
//  ShareSheetView.m
//  WPWProject
//
//  Created by 关 东波 on 14-1-24.
//  Copyright (c) 2014年 Mr.Lu. All rights reserved.
//

#import "ShareSheetView.h"
#import "ThirdPartLogin.h"
#import "WechatUtility.h"
#import "NSString+Utility.h"
#import "QQShareUtility.h"

@implementation ShareSheetView
@synthesize imageContent;
@synthesize wechatLinkUrl;
@synthesize qqThumbPosterUrl;
@synthesize qqShareTitle;
@synthesize qqLinkUrl;
@synthesize dialogLinkUrl;
@synthesize imageUrl;
@synthesize wechatTitle;


- (id)initWithImageUrl:(NSString *)mImageUrl shareFromType:(SHARE_FROM_TYPE)mShareFromType
{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        backgroundView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight+20)];
        backgroundView.userInteractionEnabled=YES;
        backgroundView.backgroundColor = RGBACOLOR(52, 52, 52, 1);
        [self addSubview:backgroundView];
        
        groupBg = [[UIImageView alloc] initWithFrame:CGRectMake(0, kDeviceHeight+20, kDeviceWidth, 280)];
        groupBg.image=[UIImage imageNamed:@"white_bg"];
        groupBg.userInteractionEnabled=YES;
        [backgroundView addSubview:groupBg];
        groupBg.center=CGPointMake(CGRectGetWidth(backgroundView.frame)/2, CGRectGetHeight(backgroundView.frame)/2);
        
        //新浪
        UIButton *sinaShareBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        sinaShareBtn.frame=CGRectMake(0, 0, CGRectGetWidth(groupBg.frame), 70);
        [sinaShareBtn addTarget:self action:@selector(shareToSina:) forControlEvents:UIControlEventTouchDown];
        [groupBg addSubview:sinaShareBtn];
        
        float width=30;
        UIImage *sinaIcon=[UIImage imageNamed:@"sheet_share_sina"];
        UIImageView *sinaIconView=[[UIImageView alloc] initWithFrame:CGRectMake(kDeviceWidth/2.0f - 77, 0, width, width)];
        sinaIconView.center=CGPointMake(sinaIconView.center.x, CGRectGetHeight(sinaShareBtn.frame)/2);
        sinaIconView.image=sinaIcon;
        [sinaShareBtn addSubview:sinaIconView];
        
        UILabel *sinaLab=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(sinaIconView.frame)+15, 0, 100, CGRectGetHeight(sinaShareBtn.frame))];
        sinaLab.font=[UIFont systemFontOfSize:14];
        sinaLab.backgroundColor = [UIColor clearColor];
        sinaLab.textColor=[UIColor colorWithRed:109/255.0 green:109/255.0 blue:109/255.0 alpha:1];
        sinaLab.text=@"新浪微博";
        sinaLab.textAlignment = NSTextAlignmentLeft;
        sinaLab.userInteractionEnabled=NO;
        [sinaShareBtn addSubview:sinaLab];
        
        UIView *line1=[[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(sinaShareBtn.frame)-0.5, CGRectGetWidth(sinaShareBtn.frame), 0.5)];
        line1.backgroundColor=DEFAULT_BORDER_COLOR;
        [sinaShareBtn addSubview:line1];
        //微信朋友圈
        UIButton *wxShareBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        wxShareBtn.frame=CGRectMake(0, CGRectGetMaxY(sinaShareBtn.frame), CGRectGetWidth(groupBg.frame), CGRectGetHeight(sinaShareBtn.frame));
        [wxShareBtn addTarget:self action:@selector(shareToWx:) forControlEvents:UIControlEventTouchDown];
        [groupBg addSubview:wxShareBtn];
        
        UIImage *wxIcon=[UIImage imageNamed:@"sheet_share_wx"];
        UIImageView *wxIconView=[[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(sinaIconView.frame), 0, width, width)];
        wxIconView.center=CGPointMake(wxIconView.center.x, CGRectGetHeight(wxShareBtn.frame)/2);
        wxIconView.image=wxIcon;
        [wxShareBtn addSubview:wxIconView];
        
        UILabel *wxLab=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(wxIconView.frame)+15, 0, 100, CGRectGetHeight(wxShareBtn.frame))];
        wxLab.font=[UIFont systemFontOfSize:14];
        wxLab.backgroundColor = [UIColor clearColor];
        wxLab.textColor=[UIColor colorWithRed:109/255.0 green:109/255.0 blue:109/255.0 alpha:1];
        wxLab.text=@"微信朋友圈";
        wxLab.textAlignment = NSTextAlignmentLeft;
        wxLab.userInteractionEnabled=NO;
        [wxShareBtn addSubview:wxLab];
        
        UIView *wxLine = [[UIView alloc]initWithFrame:CGRectMake(0,CGRectGetHeight(wxShareBtn.frame)-0.5, CGRectGetWidth(wxShareBtn.frame), 0.5)];
        wxLine.backgroundColor = DEFAULT_BORDER_COLOR;
        [wxShareBtn addSubview:wxLine];
        //微信会话
        UIButton *wxShareDialogueBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        wxShareDialogueBtn.frame = CGRectMake(0, CGRectGetMaxY(wxShareBtn.frame), CGRectGetWidth(sinaShareBtn.frame), CGRectGetHeight(sinaShareBtn.frame));
        [wxShareDialogueBtn addTarget:self action:@selector(shareToWxDialogue:) forControlEvents:UIControlEventTouchDown];
        [groupBg addSubview:wxShareDialogueBtn];
        
        UIImage *wxIcon2=[UIImage imageNamed:@"sheet_share_dialog"];
        UIImageView *wxIconView2=[[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(sinaIconView.frame),0,width, width)];
        wxIconView2.image=wxIcon2;
        wxIconView2.center=CGPointMake(wxIconView2.center.x, CGRectGetHeight(wxShareDialogueBtn.frame)/2);
        [wxShareDialogueBtn addSubview:wxIconView2];
        
        UILabel *wxLab2=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(wxIconView2.frame)+15, 0, 100, CGRectGetHeight(wxShareDialogueBtn.frame))];
        wxLab2.font=[UIFont systemFontOfSize:14];
        wxLab2.backgroundColor = [UIColor clearColor];
        wxLab2.textColor=[UIColor colorWithRed:109/255.0 green:109/255.0 blue:109/255.0 alpha:1];
        wxLab2.text=@"微信好友";
        wxLab2.textAlignment = NSTextAlignmentLeft;
        wxLab2.userInteractionEnabled=NO;
        [wxShareDialogueBtn addSubview:wxLab2];
        
        UIView *line2=[[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(wxShareDialogueBtn.frame)-0.5, CGRectGetWidth(wxShareBtn.frame), 0.5)];
        line2.backgroundColor=DEFAULT_BORDER_COLOR;
        [wxShareDialogueBtn addSubview:line2];
        //QQ
        
        
        UIButton *qqShareBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        qqShareBtn.frame=CGRectMake(0, CGRectGetMaxY(wxShareDialogueBtn.frame), CGRectGetWidth(sinaShareBtn.frame), CGRectGetHeight(sinaShareBtn.frame));
        [qqShareBtn addTarget:self action:@selector(shareToQQ:) forControlEvents:UIControlEventTouchDown];
        [groupBg addSubview:qqShareBtn];
        
        UIImage *qqIcon=[UIImage imageNamed:@"sheet_share_qq"];
        UIImageView *qqIconView=[[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(sinaIconView.frame), 0, width, width)];
        qqIconView.center=CGPointMake(qqIconView.center.x, CGRectGetHeight(qqShareBtn.frame)/2);
        qqIconView.image=qqIcon;
        [qqShareBtn addSubview:qqIconView];
        
        UILabel *qqLab=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(qqIconView.frame)+15, 0, 100, CGRectGetHeight(qqShareBtn.frame))];
        qqLab.font=[UIFont systemFontOfSize:14];
        qqLab.backgroundColor = [UIColor clearColor];
        qqLab.textColor=[UIColor colorWithRed:109/255.0 green:109/255.0 blue:109/255.0 alpha:1];
        qqLab.text=@"QQ空间";
        qqLab.textAlignment = NSTextAlignmentLeft;
        qqLab.userInteractionEnabled=NO;
        [qqShareBtn addSubview:qqLab];
        
        UIView *line3=[[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(qqShareBtn.frame)-0.5, CGRectGetWidth(qqShareBtn.frame), 0.5)];
        line3.backgroundColor=DEFAULT_BORDER_COLOR;
        [qqShareBtn addSubview:line3];
        if ([TencentOAuth iphoneQQInstalled]==NO) {
            qqShareBtn.frame=CGRectMake(CGRectGetMinX(qqShareBtn.frame), CGRectGetMinY(qqShareBtn.frame), 0, 0);
            qqShareBtn.hidden=YES;
            groupBg.frame=CGRectMake(CGRectGetMinX(groupBg.frame), CGRectGetMinY(groupBg.frame), CGRectGetWidth(groupBg.frame), CGRectGetHeight(groupBg.frame)-CGRectGetHeight(sinaShareBtn.frame));
        }
        
        imageUrl=mImageUrl;
        
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeShareSheet:)]];
        
        [[SingletonUtility instance].appDelegate.window addSubview:self];
        [ThirdPartLogin instance].delegate=self;
        
        self.frame=CGRectMake(0, kDeviceHeight+20, kDeviceWidth, kDeviceHeight);
        shareFromType=mShareFromType;
    }
    return self;
}

-(void)setFriendContent:(NSString *)mFriendsContent chatContent:(NSString *)mChatContent sinaContent:(NSString *)mSinaContent qqContent:(NSString *)mQqContent
{
    friendsContent=mFriendsContent;
    chatContent=mChatContent;
    sinaContent=mSinaContent;
    qqContent=mQqContent;
}

-(void)shareToSina:(id)sender //新浪
{
    type=Third_Part_Sina;
    if ([LocalDataUtility getValueForKey:@"sinaAccessToken"]) {
        [[ThirdPartLogin instance] loginSina];
        [self closeShareSheet:nil];
        return;
    }else{
        NSString *content=nil;
        if (shareFromType==SHARE_FROM_GAME) {
            content=sinaContent;
        }else{
            content=@"分享自 最美游戏";
        }
        if (shareFromType==SHARE_FROM_GAME) {
            [[ThirdPartLogin instance] shareContent:content imageUrl:imageUrl type:type image:[imageContent string2Image]];
        }
        [self closeShareSheet:nil];
    }
}

-(void)shareToWx:(id)sender  //微信 分享到朋友圈
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]]];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *content=nil;
            if (shareFromType==SHARE_FROM_GAME) {
                content=chatContent;
            }else{
                content=@"分享自 最美游戏";
            }
            
            if (shareFromType==SHARE_FROM_GAME){
                [WechatUtility shareWechatUrl:wechatLinkUrl image:image desc:content title:wechatTitle scene:WXSceneTimeline];
            }
            
        });
    });
}
-(void)shareToWxDialogue:(id)sender //微信 分享到回话
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]]];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *content=nil;
            if (shareFromType==SHARE_FROM_GAME){
                content=chatContent;
            }else{
                content=@"分享自 最美游戏";
            }
            
            if (shareFromType==SHARE_FROM_GAME){
                [WechatUtility shareWechatUrl:dialogLinkUrl image:image desc:content title:wechatTitle scene:WXSceneSession];
            }
        });
    });
}

-(void)shareToQQ:(id)sender
{
    type=Third_Part_QQ;
//    if ([CommonUtility isEmpty:[XYCommon myInfo].qqId]) {
//        [[ThirdPartLogin instance] loginQQ];
//        return;
//    }else{
        NSString *content=nil;
        if (shareFromType==SHARE_FROM_GAME) {
            content=qqContent;
        }else{
            content=@"分享自 最美游戏";;
        }
        
        if (shareFromType==SHARE_FROM_GAME) {
            [QQShareUtility shareWechatUrl:qqLinkUrl imageUrl:qqThumbPosterUrl desc:content title:qqShareTitle];
        }
        [self closeShareSheet:nil];
//    }
}

-(void)shareSuccess:(Third_Part_Type)mtype
{
    if (mtype==Third_Part_Sina) {
        [[SingletonUtility instance].appDelegate.window showTips:@"成功分享到新浪微博"];
    }
}

#pragma mark sina&qq bind delegate
-(void)thirdpartId:(NSString *)thirdpartId type:(Third_Part_Type)mtype nickName:(NSString *)nickName acessToken:(NSString *)accessToken
{
    
}

-(void)showShareSheet
{
    self.frame=CGRectMake(0, 0, kDeviceWidth, kDeviceHeight+20);
    groupBg.frame=CGRectMake(groupBg.left.x, kDeviceHeight+20, groupBg.width, groupBg.height);
    [UIView animateWithDuration:0.2 animations:^{
        groupBg.frame=CGRectMake(groupBg.left.x, kDeviceHeight+20-groupBg.height, groupBg.width, groupBg.height);
    } completion:^(BOOL finished) {
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(thirdPartBindRefresh) name:@"ThirdPart_Bind_Refresh" object:nil];
    }];
}

-(void)closeShareSheet:(UIGestureRecognizer *)tap
{
    [UIView animateWithDuration:0.2 animations:^{
        groupBg.frame=CGRectMake(groupBg.left.x, kDeviceHeight+20, groupBg.width, groupBg.height);
    } completion:^(BOOL finished) {
        self.frame=CGRectMake(0, kDeviceHeight+20, kDeviceWidth, kDeviceHeight+20);
        [[NSNotificationCenter defaultCenter]removeObserver:self name:@"ThirdPart_Bind_Refresh" object:nil];
    }];
}

-(void)thirdPartBindRefresh
{
    if (type == Third_Part_Sina) {
        if (![EmptyUtility isEmpty:[LocalDataUtility getValueForKey:@"sinaAccessToken"]]) {
            [self performSelector:@selector(shareToSina:) withObject:nil afterDelay:0.3];
        }
    }else if (type == Third_Part_QQ){
        if (![EmptyUtility isEmpty:[LocalDataUtility getValueForKey:@"qqAccessToken"]]) {
            [self performSelector:@selector(shareToQQ:) withObject:nil afterDelay:0.3];
        }
    }
}

@end
