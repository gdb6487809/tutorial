//
//  ShareSheetView.h
//  WPWProject
//
//  Created by 关 东波 on 14-1-24.
//  Copyright (c) 2014年 Mr.Lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ThirdPartLogin.h"
#import "UIView+Utility.h"

typedef enum
{
    SHARE_FROM_GAME = 1,
    SHARE_FROM_SUBJECT = 2,
}SHARE_FROM_TYPE;

@class ShareSheetView;
@protocol ShareSheetViewDelegate <NSObject>

@optional;
-(void)shareToThirdpartSuccess;
-(void)shareToThirdpartFailed;

@end

@interface ShareSheetView : UIView<ThirdPartLoginDelegate>
{
    UIView *backgroundView;
    UIImageView *groupBg;
    
    NSString *friendsContent;
    NSString *chatContent;
    NSString *sinaContent;
    NSString *qqContent;
    
    NSString *imageContent;
    
    NSString *imageUrl;
    Third_Part_Type thirdPartType;
    NSString *wechatLinkUrl;
    NSString *dialogLinkUrl;
    NSString *qqThumbPosterUrl;
    NSString *qqShareTitle;
    NSString *qqLinkUrl;
    int type;
    SHARE_FROM_TYPE shareFromType;
    NSString *wechatTitle;
}
@property (nonatomic,copy)NSString *imageContent;

@property (nonatomic,copy)NSString *wechatLinkUrl;
@property (nonatomic,copy)NSString *dialogLinkUrl;
@property (nonatomic,copy)NSString *qqLinkUrl;
@property (nonatomic,copy)NSString *imageUrl;
@property (nonatomic,copy)NSString *qqThumbPosterUrl;
@property (nonatomic,copy)NSString *wechatTitle;
@property (nonatomic,copy)NSString *qqShareTitle;

@property(nonatomic,assign)id<ShareSheetViewDelegate> delegate;


- (id)initWithImageUrl:(NSString *)mImageUrl shareFromType:(SHARE_FROM_TYPE)mShareFromType;
-(void)setFriendContent:(NSString *)mFriendsContent chatContent:(NSString *)mChatContent sinaContent:(NSString *)mSinaContent qqContent:(NSString *)mQqContent;
-(void)showShareSheet;


@end
