//
//  AnalysisDetailCell.m
//  analysis
//
//  Created by DongboGuan on 15/2/5.
//  Copyright (c) 2015年 DongboGuan. All rights reserved.
//

#import "AnalysisDetailCell.h"

@implementation AnalysisDetailCell
{
    UILabel *inviteAllLab;
    UILabel *topicAllLab;
    UILabel *activeAllLab;
    UILabel *orderAllLab;
    UILabel *topicLaudAllLab;
    UILabel *topicReplyAllLab;
    UILabel *filmCommentAllLab;
    UILabel *registerAllLab;
    UILabel *inviteLab;
    UIView *detailBg;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        detailBg=[[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 120)];
        detailBg.backgroundColor=[UIColor lightGrayColor];
        [self.contentView addSubview:detailBg];
        
        inviteAllLab = [[UILabel alloc] initWithFrame:CGRectMake(10,5, 200, 22)];
        inviteAllLab.backgroundColor=[UIColor clearColor];
        inviteAllLab.font=[UIFont systemFontOfSize:15];
        inviteAllLab.textColor=[UIColor darkGrayColor];
        [detailBg addSubview:inviteAllLab];
        
        topicAllLab = [[UILabel alloc] initWithFrame:CGRectMake(10,CGRectGetMaxY(inviteAllLab.frame), 200, 22)];
        topicAllLab.backgroundColor=[UIColor clearColor];
        topicAllLab.font=[UIFont systemFontOfSize:15];
        topicAllLab.textColor=[UIColor darkGrayColor];
        [detailBg addSubview:topicAllLab];
        
        activeAllLab = [[UILabel alloc] initWithFrame:CGRectMake(10,CGRectGetMaxY(topicAllLab.frame), 200, 22)];
        activeAllLab.backgroundColor=[UIColor clearColor];
        activeAllLab.font=[UIFont systemFontOfSize:15];
        activeAllLab.textColor=[UIColor darkGrayColor];
        [detailBg addSubview:activeAllLab];
        
        orderAllLab = [[UILabel alloc] initWithFrame:CGRectMake(10,CGRectGetMaxY(activeAllLab.frame), 200, 22)];
        orderAllLab.backgroundColor=[UIColor clearColor];
        orderAllLab.font=[UIFont systemFontOfSize:15];
        orderAllLab.textColor=[UIColor darkGrayColor];
        [detailBg addSubview:orderAllLab];
        
        topicLaudAllLab = [[UILabel alloc] initWithFrame:CGRectMake(10,CGRectGetMaxY(orderAllLab.frame), 200, 22)];
        topicLaudAllLab.backgroundColor=[UIColor clearColor];
        topicLaudAllLab.font=[UIFont systemFontOfSize:15];
        topicLaudAllLab.textColor=[UIColor darkGrayColor];
        [detailBg addSubview:topicLaudAllLab];
        
        topicReplyAllLab = [[UILabel alloc] initWithFrame:CGRectMake(10,CGRectGetMaxY(topicLaudAllLab.frame), 200, 22)];
        topicReplyAllLab.backgroundColor=[UIColor clearColor];
        topicReplyAllLab.font=[UIFont systemFontOfSize:15];
        topicReplyAllLab.textColor=[UIColor darkGrayColor];
        [detailBg addSubview:topicReplyAllLab];
        
        filmCommentAllLab = [[UILabel alloc] initWithFrame:CGRectMake(10,CGRectGetMaxY(topicReplyAllLab.frame), 200, 22)];
        filmCommentAllLab.backgroundColor=[UIColor clearColor];
        filmCommentAllLab.font=[UIFont systemFontOfSize:15];
        filmCommentAllLab.textColor=[UIColor darkGrayColor];
        [detailBg addSubview:filmCommentAllLab];
        
        registerAllLab = [[UILabel alloc] initWithFrame:CGRectMake(10,CGRectGetMaxY(filmCommentAllLab.frame), 200, 22)];
        registerAllLab.backgroundColor=[UIColor clearColor];
        registerAllLab.font=[UIFont systemFontOfSize:15];
        registerAllLab.textColor=[UIColor darkGrayColor];
        [detailBg addSubview:registerAllLab];
        
        inviteLab = [[UILabel alloc] initWithFrame:CGRectMake(10,CGRectGetMaxY(registerAllLab.frame), 200, 22)];
        inviteLab.backgroundColor=[UIColor clearColor];
        inviteLab.font=[UIFont systemFontOfSize:15];
        inviteLab.textColor=[UIColor darkGrayColor];
        [detailBg addSubview:inviteLab];
    }
    return self;
}

-(void)initWithAnalysisModel:(AnalysisModel *)model index:(NSInteger)_index
{
    if (model.opened) {
        inviteAllLab.text=[NSString stringWithFormat:@"有效约会：%@",@(model.inviteallcount)];
        topicAllLab.text=[NSString stringWithFormat:@"发帖数量：%@",@(model.interesttopiccount)];
        activeAllLab.text=[NSString stringWithFormat:@"注册活跃：%@",@(model.activecount)];
        orderAllLab.text=[NSString stringWithFormat:@"购票数量（含未支付）：%@",@(model.ordercount)];
        topicLaudAllLab.text=[NSString stringWithFormat:@"帖子点赞数量：%@",@(model.interesttopiclaudcount)];
        topicReplyAllLab.text=[NSString stringWithFormat:@"帖子回复数量：%@",@(model.interesttopicreplycount)];
        filmCommentAllLab.text=[NSString stringWithFormat:@"影评数量：%@",@(model.filmcriticcount)];
        registerAllLab.text=[NSString stringWithFormat:@"注册数量：%@",@(model.registcount)];
        inviteLab.text=[NSString stringWithFormat:@"约会新增：%@",@(model.invitecount)];
        detailBg.frame=CGRectMake(0, 0, kDeviceWidth, CGRectGetMaxY(inviteLab.frame)+5);
        self.frame=CGRectMake(0, 0, kDeviceWidth, CGRectGetMaxY(detailBg.frame));
    }else{
        inviteAllLab.text=@"";
        detailBg.frame=CGRectZero;
        self.frame=CGRectZero;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
