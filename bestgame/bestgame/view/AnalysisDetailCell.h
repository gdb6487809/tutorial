//
//  AnalysisDetailCell.h
//  analysis
//
//  Created by DongboGuan on 15/2/5.
//  Copyright (c) 2015年 DongboGuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnalysisModel.h"

@interface AnalysisDetailCell : UITableViewCell

-(void)initWithAnalysisModel:(AnalysisModel *)model index:(NSInteger)_index;

@end
